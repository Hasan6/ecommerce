from rest_framework import serializers
from django.contrib.auth.models import User, Group
from users.models import User


class SellerRegisterSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'full_name', 'password', 'email', 'phone', 'gender', 'address', 'store_address', 'store_name', 'zip_code')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        my_group = Group.objects.get(name='seller')
        user = User.objects.create_user(**validated_data)
        my_group.user_set.add(user)
        return user
