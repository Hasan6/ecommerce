from django.urls import path
from . import views
from .views import SellerRegisterAPIView


urlpatterns = [
    path("seller/", views.index, name="seller.index"),
    # path("login", views.login, name="seller_login"),
    path('api/seller/registration/', SellerRegisterAPIView.as_view(), name="Seller_reg"),
]