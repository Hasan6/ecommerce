from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from seller.serializers import SellerRegisterSerializer
from rest_framework.permissions import AllowAny
# Create your views here.


@login_required(login_url='/login/')
def index(request):
    return render(request, 'seller/seller.html', {})


class SellerRegisterAPIView(APIView):
    permission_classes = (AllowAny,)
    serializer_class = SellerRegisterSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)

        # print(serializer)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'User registered  successfully',
        }
        status_code = status.HTTP_200_OK
        return Response(response, status=status_code)