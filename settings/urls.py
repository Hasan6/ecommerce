from django.urls import path
from .views import VatAmountAPIView, CommissionAmountAPIView, KeywordAPIView

urlpatterns = [
    path('api/vat/amount', VatAmountAPIView.as_view()),
    path('api/commission/amount', CommissionAmountAPIView.as_view()),
    path('api/keyword/list/', KeywordAPIView.as_view()),
]