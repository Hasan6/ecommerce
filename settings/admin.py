from django.contrib import admin
from settings.models import Brand, Unit, Color, Vat, Commission, Keyword, PaytabConfig

# Register your models here.
class VatAdmin(admin.ModelAdmin):
    list_display = ('percentage',)

    def has_delete_permission(self, request, obj=None):
        # Disable delete
        return False
class PaytabAdmin(admin.ModelAdmin):
    list_display = ('profile_id', 'authorization', 'returns')

    def has_delete_permission(self, request, obj=None):
        # Disable delete
        return False
class KeywordAdmin(admin.ModelAdmin):
    list_display = ('name',)


class ComissionAdmin(admin.ModelAdmin):
    list_display = ('percentage',)

    def has_delete_permission(self, request, obj=None):
        # Disable delete
        return False
admin.site.register(Brand)
admin.site.register(Unit)
admin.site.register(Color)
admin.site.register(Vat,VatAdmin)
admin.site.register(Commission,ComissionAdmin)
admin.site.register(Keyword, KeywordAdmin)
admin.site.register(PaytabConfig, PaytabAdmin)