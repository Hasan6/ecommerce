from django.shortcuts import render
from settings.serializers import VatSerializer, CommissionSerializer, KeywordSerializer
from settings.models import Vat, Commission, Keyword
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.decorators import login_required
from rest_framework.permissions import AllowAny


# Create your views here.
class VatAmountAPIView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):

        try:
            vat = Vat.objects.get(id=1)
        except Vat.DoesNotExist:
            response = {
                'success': 'True',
                'status code': status.HTTP_404_NOT_FOUND,
                'message': 'Vat is Not Found'
            }
            return Response(response)
        serializer = VatSerializer(vat, many=False)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Vat',
            'data': [serializer.data]
        }
        return Response(response)


class CommissionAmountAPIView(APIView):
    def get(self, request):

        try:
            commission_info = Commission.objects.get(id=1)
        except Commission.DoesNotExist:
            response = {
                'success': 'True',
                'status code': status.HTTP_404_NOT_FOUND,
                'message': 'Commission is Not Found'
            }
            return Response(response)
        serializer = CommissionSerializer(commission_info, many=False)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Commission',
            'data': [serializer.data]
        }
        return Response(response)


class KeywordAPIView(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Keyword List',
            'data': []
        }
        key = Keyword.objects.all()
        serializer = KeywordSerializer(key, many=True)
        response['data'] = serializer.data
        return Response(response)
