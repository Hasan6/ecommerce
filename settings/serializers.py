from rest_framework import serializers
from settings.models import Vat, Commission, Keyword


class VatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Vat
        fields = ['percentage']


class CommissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Commission
        fields = ['percentage']


class KeywordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Keyword
        fields = '__all__'
