from django.urls import path
from .views import  SellerWiseOrderDetailsAPIView, OrderListBySellerAPI, OrderAddAPIView, OrderTrackingAPIView, OrderTrackAPIView, PaymentTransactionAPIView,PaymentCheckAPIView, PaymentRefundAPIView, OrderListByBuyerAPI, OrderDetailsByBuyerAPI, AddressBookAddAPIView, AddressBookListAPIView, AddressBookDefaultChangeAPIView, AddressBookEditAPIView, AddressBookDefaultAPIView

urlpatterns = [
    path('api/address/book/add/', AddressBookAddAPIView.as_view()),
    path('api/address/book/edit/', AddressBookEditAPIView.as_view()),
    path('api/address/book/list/<str:pk>/', AddressBookListAPIView.as_view()),
    path('api/address/book/default/<str:pk>/', AddressBookDefaultAPIView.as_view()),
    path('api/address/book/default/change/', AddressBookDefaultChangeAPIView.as_view()),
    path('api/order/add/', OrderAddAPIView.as_view()),
    path('api/order/details/<str:seller>/<str:order>/', SellerWiseOrderDetailsAPIView.as_view()),
    path('api/order/list/<str:pk>/', OrderListBySellerAPI.as_view()),
    path('api/buyer/order/list/<str:pk>/', OrderListByBuyerAPI.as_view()),
    path('api/buyer/order/details/<str:pk>/', OrderDetailsByBuyerAPI.as_view()),
    path('api/order/update/tracking/status/<str:pk>/', OrderTrackingAPIView.as_view()),
    path('api/order/track/info/', OrderTrackAPIView.as_view()),
    path('api/order/payment/trans/', PaymentTransactionAPIView.as_view()),
    path('api/order/payment/check/', PaymentCheckAPIView.as_view()),
    path('api/order/payment/refund/', PaymentRefundAPIView.as_view()),
]