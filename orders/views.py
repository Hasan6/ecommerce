from django.http import JsonResponse
from django.shortcuts import render
# from jet.utils import JsonResponse

from orders.serializers import OrderDetailSerializer, OrderAddSerializer, OrderTrackingUpdateSerializer, OrderTrackingListSerializer, OrderTrackSerializer, OrderSerializer, AddressBookAddSerializer, AddressBookListSerializer
from orders.models import Order, OrderDetail, OrderTracking, Transaction, AddressBook
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
import requests
from rest_framework.permissions import IsAuthenticated, AllowAny
from settings.models import Vat, Commission, PaytabConfig
from users.models import City
import random,json
from datetime import datetime

try:
    default_vat = Vat.objects.get(pk=1)
except:
    default_vat = 0


try:
    default_commission = Commission.objects.get(pk=1)
except:
    default_commission = 0



class AddressBookListAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request, pk):
        if self.request.user.id != int(pk):
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'You can not show this list!!'
            }
            return Response(response)
        buyer_id = pk
        address = AddressBook.objects.filter(buyer=buyer_id)
        serializer = AddressBookListSerializer(address,many=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'buyer wise address list',
            'data': serializer.data
        }
        return Response(response)

class AddressBookDefaultAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request, pk):
        if self.request.user.id != int(pk):
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'You can not show this information!!'
            }
            return Response(response)
        buyer_id = pk
        address = AddressBook.objects.filter(buyer=buyer_id,is_default=1)
        serializer = AddressBookListSerializer(address,many=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Buyer default address list',
            'data': serializer.data
        }
        return Response(response)

class AddressBookDefaultChangeAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self,request):
        address_id = request.data['id']
        buyer = request.data['buyer']
        AddressBook.objects.filter(buyer=buyer).update(is_default=0)
        AddressBook.objects.filter(id=address_id).update(is_default=1)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Default address change successful'
        }
        return Response(response)


class SellerWiseOrderDetailsAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request, seller, order):
        seller_id = seller
        if self.request.user.id != int(seller_id):
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'You can not show this list!!'
            }
            return Response(response)
        orders = OrderDetail.objects.filter(seller=seller_id, order=order)
        serializer = OrderDetailSerializer(orders,many=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Seller wise order Details',
            'data': serializer.data
        }
        return Response(response)

class OrderDetailsByBuyerAPI(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request, pk):
        order_id = pk
        orders = OrderDetail.objects.filter(order=order_id)
        serializer = OrderDetailSerializer(orders,many=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Buyer wise order Details',
            'data': serializer.data
        }
        return Response(response)


class OrderListBySellerAPI(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request, pk):
        if self.request.user.id != int(pk):
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'You can not show this list!!'
            }
            return Response(response)
        seller_id = pk
        orders = OrderTracking.objects.filter(seller=seller_id)
        serializer = OrderTrackSerializer(orders,many=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Seller wise order list',
            'data': serializer.data
        }
        return Response(response)

class OrderListByBuyerAPI(APIView):
    permission_classes = (AllowAny,)
    def get(self,request, pk):
        if self.request.user.id != int(pk):
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'You can not show this list!!'
            }
            return Response(response)
        buyer_id = pk
        orders = Order.objects.filter(buyer=buyer_id)
        serializer = OrderSerializer(orders,many=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Buyer wise order list',
            'data': serializer.data
        }
        return Response(response)

def random_string():
    return str(random.randint(10000, 99999))



class AddressBookAddAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        serializer = AddressBookAddSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'Address Insert Successful',
                'data': [serializer.data]
            }
            return Response(response)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class AddressBookEditAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        pk = request.data['id']
        city = request.data['city']
        city_info = City.objects.get(id=city)
        country = city_info.country_id
        address = request.data['address']
        AddressBook.objects.filter(id=pk).update(city_id=city, country_id=country, address=address)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Address Update Successful'
        }
        return Response(response)

class OrderAddAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):

        request.data['seller_payment_status'] = 1
        request.data['buyer_payment_status'] = 1
        request.data['order_datetime'] = datetime.today()
        request.data['is_approved'] = 1



        serializer = OrderAddSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'Order Insert Successful',
                'data': [serializer.data]
            }
            return Response(response)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class OrderTrackingAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request, pk):
        if self.request.user.id != int(pk):
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'You can not show this information!!'
            }
            return Response(response)
        order_id = request.data['order']
        order_tracking = OrderTracking.objects.get(seller=pk, order=order_id)
        serializer = OrderTrackingUpdateSerializer(order_tracking, data=request.data)
        if serializer.is_valid():
            serializer.save()
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'Order Track Update Successful',
                'data': [serializer.data]
            }
            return Response(response)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class OrderTrackAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        order_id = request.data['order']
        seller_id = request.data['seller']

        track_info = OrderTracking.objects.get(order=order_id, seller=seller_id)
        serializer = OrderTrackingListSerializer(track_info,data=request.data)
        if serializer.is_valid():
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'Order Track Information',
                'data': serializer.data
            }
            return Response(response)
class PaymentTransactionAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):

        url = 'https://secure.paytabs.sa/payment/request'

        paytab = PaytabConfig.objects.get(id=1)


        profile_id = paytab.profile_id
        tran_class = paytab.tran_class
        callback = paytab.callback
        returns = paytab.returns
        cart_currency = request.data['cart_currency']
        cart_amount = request.data['cart_amount']
        cart_id = request.data['cart_id']
        cart_description = request.data['cart_description']
        tran_type = request.data['tran_type']


        name = request.data['customer_details']['name']
        email = request.data['customer_details']['email']
        phone = request.data['customer_details']['phone']
        street1 = request.data['customer_details']['street1']
        city = request.data['customer_details']['city']
        state = request.data['customer_details']['state']
        country = request.data['customer_details']['country']
        zip_code = request.data['customer_details']['zip_code']
        ip = request.data['customer_details']['ip']

        customer_details = {'name': name, 'email': email,'phone': phone,'street1': street1,'city': city,'state': state,'country': country, 'zip_code':zip_code ,'ip': ip}

        myjson = {'profile_id': profile_id, 'tran_class': tran_class, 'tran_type': tran_type, 'callback': callback, 'return': returns,
                  'cart_currency': cart_currency, 'cart_amount': cart_amount, 'cart_id': cart_id,
                  'cart_description': cart_description, 'customer_details': customer_details,'shipping_details' : customer_details}


        r = requests.post(url=url, data=json.dumps(myjson),
                     headers={'Content-Type':'application/json','authorization': paytab.authorization})
        conv = r.json()



        return Response(conv)

class PaymentCheckAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        url = 'https://secure.paytabs.sa/payment/query'
        paytab = PaytabConfig.objects.get(id=1)
        profile_id = paytab.profile_id


        tran_ref = request.data['tran_ref']
        if 'order' not in request.data:
            order = ''
        else:
            order = request.data['order']

        if 'subscription' not in request.data:
            subscription = ''
        else:
            subscription = request.data['subscription']

        cart_currency = request.data['cart_currency']
        cart_amount = request.data['cart_amount']
        cart_id = request.data['cart_id']
        cart_description = request.data['cart_description']
        tran_type = request.data['tran_type']


        verify = {'profile_id':profile_id, 'tran_ref':tran_ref}

        r = requests.post(url=url, data=json.dumps(verify),
                          headers={'Content-Type': 'application/json', 'authorization': paytab.authorization})
        conv = r.json()

        # print(conv)

        # print(conv['payment_result']['response_message'])
        if 'code' not in conv:
            if conv['payment_result']['response_message'] == 'Authorised':
                if order:
                    Order.objects.filter(id=order).update(buyer_payment_status=1)

                trans = Transaction(order_id=order, subscription_id=subscription, tran_type=tran_type, transaction_ref=conv['tran_ref'],
                                    cart_description=cart_description, cart_id=cart_id, cart_currency=cart_currency,
                                    cart_amount=cart_amount)
                trans.save()
                response = {
                    'success': 'True',
                    'status code': status.HTTP_200_OK,
                    'message': 'Payment Success',
                    'data': [conv]
                }
            else:
                response = {
                    'success': 'False',
                    'status code': status.HTTP_200_OK,
                    'message': 'Payment Rejected',
                    'data': [conv]
                }
        #
        # if 'tran_ref' in request.session:
            # del request.session['tran_ref']
            # print(request.session['tran_ref'])
            return JsonResponse(response)
        else:
            response = {
                'success': 'False',
                'status code': status.HTTP_200_OK,
                'message': 'Invalid transaction reference',
                'data': [conv]
            }
            return JsonResponse(response)

class PaymentRefundAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        url = 'https://secure.paytabs.sa/payment/request'
        paytab = PaytabConfig.objects.get(id=1)
        profile_id = paytab.profile_id
        tran_class = paytab.tran_class


        tran_ref = request.data['tran_ref']


        transaction_info = Transaction.objects.get(transaction_ref=tran_ref)
        cart_currency = transaction_info.cart_currency
        cart_amount = transaction_info.cart_amount
        cart_id = transaction_info.cart_id
        cart_description =transaction_info.cart_description
        tran_type = 'refund'
        order = transaction_info.order_id
        subscription = transaction_info.subscription_id

        refund =  {"profile_id": profile_id, "cart_id": cart_id, "tran_class": tran_class, "cart_description": cart_description, "tran_ref": tran_ref, "cart_currency": cart_currency,  "tran_type": tran_type, "cart_amount": cart_amount}

        # print(refund)
        r = requests.post(url=url, data=json.dumps(refund),
                          headers={'Content-Type': 'application/json', 'authorization': paytab.authorization})
        conv = r.json()

        # print(conv)

        # print(conv['payment_result']['response_message'])
        if 'code' not in conv:
            if conv['payment_result']['response_message'] == 'Authorised':
                trans = Transaction(order_id=order, subscription_id=subscription, tran_type=tran_type, transaction_ref=conv['tran_ref'],
                                    cart_description=cart_description, cart_id=cart_id, cart_currency=cart_currency,
                                    cart_amount=cart_amount)
                trans.save()
                response = {
                    'success': 'True',
                    'status code': status.HTTP_200_OK,
                    'message': 'Payment Success',
                    'data': [conv]
                }
            else:
                response = {
                    'success': 'False',
                    'status code': status.HTTP_200_OK,
                    'message': 'Refund Rejected',
                    'data': [conv]
                }
        #
        # if 'tran_ref' in request.session:
            # del request.session['tran_ref']
            # print(request.session['tran_ref'])
            return JsonResponse(response)
        else:
            response = {
                'success': 'False',
                'status code': status.HTTP_200_OK,
                'message': 'Unable to process your request',
                'data': [conv]
            }
            return JsonResponse(response)