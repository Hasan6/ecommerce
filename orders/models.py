from django.core.mail import send_mail
from django.core.cache import caches
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from .tasks import buyer_order_confirmation_email,seller_order_confirmation_email
from products.models import Product
from settings.models import Vat, Commission
from subscriptions.models import Subscription
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from emdad.settings import EMAIL_HOST_USER
import random
import sys
paymentstatus = (
    (1, 'Paid'),
    (0, 'Unpaid'),
)
payment_type = (
    (0, 'Wire Transfer'),
    (1, 'Card'),
)

approve = (
    (0, 'Approved'),
    (1, 'Unapproved'),
)

is_address = (
    (0, 'No'),
    (1, 'Yes'),
)
tracking_status = (
    (1, 'Created'),
    (2, 'confirmed'),
    (3, 'processing'),
    (4, 'delivered'),
)


# Create your models here.

class AddressBook(models.Model):
    buyer = models.ForeignKey(
        'users.User',
        related_name="address_book_buyer",
        on_delete=models.CASCADE
    )
    country = models.ForeignKey(
        'users.Country',
        related_name="address_book_country",
        on_delete=models.CASCADE, blank=True, null=True
    )

    city = models.ForeignKey(
        'users.City',
        related_name="address_book_city",
        on_delete=models.CASCADE, blank=False
    )
    zip_code = models.CharField(max_length=100, null=True, blank=True)
    address = models.TextField(blank=True, null=True)
    is_default = models.IntegerField(choices=is_address, default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        'users.User',
        related_name="address_book_created_by",
        on_delete=models.CASCADE, blank=True, null=True
    )
    updated_by = models.ForeignKey(
        'users.User',
        related_name="address_book_updated_by",
        on_delete=models.CASCADE, blank=True, null=True
    )

    def __str__(self):
        return "%s" % self.country


def random_string():
    return str(random.randint(10000, 99999))


class Order(models.Model):
    order_code = models.CharField(max_length=250, unique=True, default=random_string)
    total_amount = models.DecimalField(max_digits=18, decimal_places=2)
    payment_type = models.IntegerField(choices=payment_type, default=1)
    seller_payment_status = models.IntegerField(choices=paymentstatus, default=0)
    buyer = models.ForeignKey(
        'users.User',
        related_name="order_buyer",
        on_delete=models.CASCADE, null=False, blank=False
    )
    buyer_payment_status = models.IntegerField(choices=paymentstatus, default=0)
    image = models.ImageField(upload_to='uploads/order/images/', blank=True, null=True)
    order_datetime = models.DateTimeField(auto_now_add=True)
    is_approved = models.IntegerField(choices=approve, default=1)
    discount_coupon_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    discount_coupon = models.ForeignKey(
        'coupons.Coupon',
        related_name="order_coupons",
        on_delete=models.CASCADE, blank=True,
        verbose_name='Coupon',
        null=True
    )
    created_by = models.ForeignKey(
        'users.User',
        related_name="order_created",
        on_delete=models.CASCADE, null=True, blank=True
    )
    updated_by = models.ForeignKey(
        'users.User',
        related_name="order_updated",
        on_delete=models.CASCADE, null=True, blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_deleted = models.BooleanField(default=False)

    class Meta:
        ordering = ('created_at',)
        verbose_name = 'order'
        verbose_name_plural = 'orders'

    def __str__(self):
        return "%s" % self.order_code


class OrderDetail(models.Model):
    order = models.ForeignKey(
        'Order',
        related_name="orders_details",
        on_delete=models.CASCADE, blank=False
    )
    product = models.ForeignKey(
        'products.Product',
        related_name="orders_product",
        on_delete=models.CASCADE, blank=False
    )

    seller = models.ForeignKey(
        'users.User',
        related_name="order_seller",
        on_delete=models.CASCADE, null=False, blank=False
    )

    shipping_address = models.ForeignKey(
        'AddressBook',
        related_name="shipping_address",
        on_delete=models.CASCADE, blank=True, null=True
    )

    pickup_address = models.ForeignKey(
        'products.PickupAddress',
        related_name="pickup_addresses",
        on_delete=models.CASCADE, blank=True, null=True
    )
    quantity = models.DecimalField(max_digits=10, decimal_places=2)
    unit_price = models.DecimalField(max_digits=10, decimal_places=2)
    commission = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    vat_amount = models.DecimalField(max_digits=10, decimal_places=2, default=0)


class OrderTracking(models.Model):
    order = models.ForeignKey(
        'Order',
        related_name="tracking_order",
        on_delete=models.CASCADE, blank=False
    )
    order_created_by = models.ForeignKey(
        'users.User',
        related_name="tracking_order_created",
        on_delete=models.CASCADE, null=True, blank=True
    )
    seller = models.ForeignKey(
        'users.User',
        related_name="order_tracking_seller",
        on_delete=models.CASCADE, null=False, blank=False
    )
    order_creation_date = models.DateTimeField(auto_now_add=True)
    order_confirmed_by = models.ForeignKey(
        'users.User',
        related_name="tracking_order_confirmed",
        on_delete=models.CASCADE, null=True, blank=True
    )
    order_confirmed_date = models.DateField(null=True, blank=True)
    shipping_processing_by = models.ForeignKey(
        'users.User',
        related_name="tracking_shipping_process",
        on_delete=models.CASCADE, null=True, blank=True
    )
    shipping_processing_date = models.DateField(null=True, blank=True)
    delivery_by_courier_name = models.CharField(max_length=250, unique=False, null=True, blank=True)
    delivery_date = models.DateTimeField(null=True, blank=True)
    status = models.IntegerField(choices=tracking_status, default=1)


class Transaction(models.Model):
    order = models.ForeignKey(
        Order,
        related_name="order_payment",
        on_delete=models.CASCADE, blank=True, null=True
    )
    transaction_ref = models.CharField(max_length=200)
    tran_type = models.CharField(max_length=100)
    cart_description = models.CharField(max_length=200)
    cart_id = models.CharField(max_length=200)
    cart_currency = models.CharField(max_length=200)
    cart_amount = models.CharField(max_length=200)
    status = models.BooleanField(default=False)
    subscription = models.ForeignKey(
        Subscription,
        related_name="subscription_payment",
        on_delete=models.CASCADE, blank=True, null=True
    )
    created_at = models.DateTimeField(auto_now_add=True)


@receiver(post_save, sender=Order)
def post_save_buyer_order_confirm(sender, instance, **kwargs):
    #buyer_order_confirmation_email.delay(email=instance.buyer.email, date=instance.created_at)
    subject = 'Order Confirmed!'
    html_message = render_to_string('mail_template/order_confirm_for_buyer.html', {'date': instance.created_at.strftime("%m-%d-%Y %I:%M:%S %p")})
    plain_message = strip_tags(html_message)
    email_from = EMAIL_HOST_USER
    recipient_list = [instance.buyer.email, ]
    try:
        send_mail(subject, plain_message, email_from, recipient_list, html_message=html_message)
    except Exception as e:
        error = 'on line {}'.format(sys.exc_info()[-1].tb_lineno), e
        print(f"Error:{error}")


@receiver(post_save, sender=OrderTracking)
def post_save_seller_order_confirm(sender, instance, **kwargs):
    #seller_order_confirmation_email.delay(email=instance.seller.email, date=instance.order_creation_date)
    subject = 'Order Confirmed!'
    html_message = render_to_string('mail_template/order_confirm_for_seller.html', {'date': instance.order_creation_date.strftime("%m-%d-%Y %I:%M:%S %p")})
    plain_message = strip_tags(html_message)
    email_from = EMAIL_HOST_USER
    recipient_list = [instance.seller.email, ]
    try:
        send_mail(subject, plain_message, email_from, recipient_list, html_message=html_message)
    except Exception as e:
        error = 'on line {}'.format(sys.exc_info()[-1].tb_lineno), e
        print(f"Error:{error}")
