from django.contrib import admin
from orders.models import AddressBook, Order, OrderDetail, OrderTracking
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter



class OrderDetailsAdmin(admin.StackedInline):
    model = OrderDetail
    readonly_fields = ('commission','vat_amount')


class OrderAdmin(admin.ModelAdmin):
    inlines = [OrderDetailsAdmin]

    exclude = ['created_by', 'updated_by']
    list_display = ('order_code', 'buyer_payment_status', 'seller_payment_status','created_at',)
    list_filter = ('is_approved', ('created_at', DateRangeFilter))
    def save_model(self, request, obj, form, change):
        if not obj.id:
            obj.created_by = request.user
            obj.updated_by = request.user
        if change and obj.id:
            obj.updated_by = request.user
        obj.save()


class AddressBookAdmin(admin.ModelAdmin):
    exclude = ['created_by', 'updated_by']
    list_display = ('buyer','country','address','created_at',)

    def save_model(self, request, obj, form, change):
        if not obj.id:
            obj.created_by = request.user
            obj.updated_by = request.user
        if change and obj.id:
            obj.updated_by = request.user
        obj.save()





class OrderTrackingAdmin(admin.ModelAdmin):
    exclude = ['order_created_by', 'order_confirmed_by','order_creation_date']
    list_display = ('order_created_by','order_confirmed_by','delivery_date','status',)
    list_filter = ('status', ('delivery_date', DateRangeFilter))

    def save_model(self, request, obj, form, change):
        if not obj.id:
            obj.order_created_by = request.user
            obj.order_confirmed_by = request.user
        # if change and obj.id:
        #     obj.updated_by = request.user
        obj.save()


admin.site.register(Order, OrderAdmin)
admin.site.register(AddressBook, AddressBookAdmin)
admin.site.register(OrderTracking, OrderTrackingAdmin)

