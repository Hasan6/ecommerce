from rest_framework import serializers
from orders.models import Order, OrderDetail, AddressBook, OrderTracking, Transaction
from products.models import Product, PickupAddress
from users.models import User, City, Country



class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = ['name','phone_code','iso3','iso2']

class CitySerializer(serializers.ModelSerializer):
    country = CountrySerializer()
    class Meta:
        model = City
        fields = ['name','country']


class AddressBookAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = AddressBook
        fields = ['buyer', 'country', 'city', 'zip_code', 'address']

class AddressBookListSerializer(serializers.ModelSerializer):
    city = CitySerializer()
    class Meta:
        model = AddressBook
        fields = ['id','buyer', 'city', 'address', 'zip_code', 'is_default']

class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ['id', 'transaction_ref', 'tran_type', 'cart_description', 'created_at', 'cart_amount', 'cart_currency']



class SellerSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'full_name']

class AddressBookSerializer(serializers.ModelSerializer):
    class Meta:
        model = AddressBook
        fields = ['id', 'country', 'city', 'is_default', 'zip_code', 'address']



class PickupAddressSerializer(serializers.ModelSerializer):
    city = CitySerializer()
    class Meta:
        model = PickupAddress
        fields = ['city', 'address']

class ProductSerializer(serializers.ModelSerializer):
    pickup_address = PickupAddressSerializer(many=True)
    class Meta:
        model = Product
        fields = ['name', 'pickup_address', 'unit_price', 'delivery_method', 'ddp_lead_time', 'ex_works_lead_time', 'stock_quantity', 'image1']

class OrdersSerializer(serializers.ModelSerializer):
    order_payment = TransactionSerializer(many=True)
    class Meta:
        model = Order
        fields = ['id', 'order_payment', 'order_code', 'buyer', 'total_amount', 'payment_type', 'seller_payment_status', 'buyer_payment_status', 'image','order_datetime','discount_coupon_amount']

class OrderTrackingListSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderTracking
        fields = ['id', 'seller', 'order', 'order_creation_date', 'order_confirmed_date', 'shipping_processing_by', 'shipping_processing_date', 'delivery_by_courier_name', 'delivery_date', 'status']

class OrderDetailSerializer(serializers.ModelSerializer):
    product = ProductSerializer()
    order = OrdersSerializer()
    class Meta:
        model = OrderDetail
        fields = ['order', 'product', 'seller', 'shipping_address', 'pickup_address', 'quantity', 'unit_price', 'vat_amount', 'commission']

class OrderTrackingUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderTracking
        fields = ['order_confirmed_by', 'order_confirmed_date', 'shipping_processing_by', 'shipping_processing_date', 'delivery_by_courier_name', 'delivery_date', 'status']




class OrderSerializer(serializers.ModelSerializer):
    order_payment = TransactionSerializer(many=True)
    tracking_order = OrderTrackingUpdateSerializer(many=True)
    class Meta:
        model = Order
        fields = ['id', 'order_payment', 'tracking_order', 'order_code', 'buyer', 'total_amount', 'payment_type', 'seller_payment_status', 'buyer_payment_status', 'image','order_datetime','discount_coupon_amount']


class OrderTrackSerializer(serializers.ModelSerializer):
    order = OrdersSerializer()
    class Meta:
        model = OrderTracking
        fields = ['id', 'seller', 'order', 'order_creation_date', 'order_confirmed_date', 'shipping_processing_by', 'shipping_processing_date', 'delivery_by_courier_name', 'delivery_date', 'status']



class OrderDetailsAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetail
        fields = ['product', 'seller', 'shipping_address', 'pickup_address', 'quantity', 'unit_price', 'vat_amount', 'commission']

class OrderTrackingSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderTracking
        fields = ['seller', 'order_created_by', 'order_creation_date', 'status']

class OrderAddSerializer(serializers.ModelSerializer):
    orders_details = OrderDetailsAddSerializer(many=True)
    tracking_order = OrderTrackingSerializer(many=True)
    class Meta:
        model = Order
        fields = ['id', 'order_code', 'orders_details', 'tracking_order', 'buyer', 'total_amount', 'payment_type', 'seller_payment_status', 'buyer_payment_status', 'image', 'order_datetime','discount_coupon_amount', 'discount_coupon']

    def create(self, validated_data):

        order_details_data = validated_data.pop('orders_details')
        order_tracking_data = validated_data.pop('tracking_order')
        order_data = Order.objects.create(**validated_data)

        for order_details in order_details_data:
            OrderDetail.objects.create(order=order_data, **order_details)
            product = Product.objects.get(id=order_details['product'].id)
            product.stock_quantity = product.stock_quantity-order_details['quantity']
            product.save()

        for order_track in order_tracking_data:
            OrderTracking.objects.create(order=order_data, **order_track)

        return order_data

