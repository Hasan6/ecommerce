from rest_framework import serializers
from rest_framework_recursive.fields import RecursiveField
from .models import Category


# class CategorySerializer(serializers.ModelSerializer):
#     parent = RecursiveField(allow_null=True)
#     class Meta:
#         model = Category
#         fields = ('name', 'parent','children')


class CategorySerializer(serializers.ModelSerializer):
    children = RecursiveField(many=True, required=False)

    # leaf_nodes = RecursiveField(many=True, required=False)
    # full_name = SerializerMethodField("get_full_name")
    # group_count = serializers.Field(source='get_group_count')

    class Meta:
        model = Category
        fields = ('id', 'name', 'icon', 'children',)

    # def get_leaf_nodes(self, obj):
    #     return CategorySerializer(obj.get_children(), many=True).data
    # def get_full_name(self, obj):
    #     name = obj.name
    #
    #     if "parent" in self.context:
    #         parent = self.context["parent"]
    #
    #         parent_name = self.context["parent_serializer"].get_full_name(parent)
    #
    #         name = "%s - %s" % (parent_name, name, )
    #
    #     return name
