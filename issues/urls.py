from django.urls import path
from .views import IssueAddAPIView, IssueListAPIView
urlpatterns = [
    path('api/issue/add/', IssueAddAPIView.as_view()),
    path('api/issue/list/<str:pk>/', IssueListAPIView.as_view()),
]