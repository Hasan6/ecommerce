from django.db import models
import random


status = (
    (0, 'Initiative'),
    (1, 'Undergoing'),
    (2, 'Resolved'),
)

type = (
    (0, 'Issue'),
    (1, 'Suggest Products'),
)

def random_string():
    return str(random.randint(10000, 99999))


class Issue(models.Model):
    issue_code = models.CharField(max_length=250, unique=True, default=random_string)
    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    order_code = models.CharField(max_length=255,  blank=True, null=True)
    status = models.IntegerField(choices=status, default=0)
    type = models.BooleanField(choices=type, default=0)
    user = models.ForeignKey(
        'users.User',
        related_name="issue_user",
        on_delete=models.CASCADE
    )
    issue_date = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='uploads/issue/images/', blank=True, null=True)


    def __str__(self):
        return "%s" % self.title

class IssueDetail(models.Model):
    issue = models.ForeignKey(
        Issue,
        related_name="issues",
        on_delete=models.CASCADE
    )
    message = models.TextField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        'users.User',
        related_name="issue_created_by",
        on_delete=models.CASCADE, blank=True, null=True
    )
    updated_at = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(
        'users.User',
        related_name="issue_updated_by",
        on_delete=models.CASCADE, blank=True, null=True
    )

    def __str__(self):
        return "%s" % self.message