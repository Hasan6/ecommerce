from django.contrib import admin
from issues.models import Issue, IssueDetail
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter

class IssueDetailAdmin(admin.StackedInline):
    model = IssueDetail

    exclude = ['created_by', 'updated_by','created_at', 'updated_at']

    def save_model(self, request, obj, form, change):
        if not obj.id:
            obj.created_by = request.user
            obj.updated_by = request.user
        # if change and obj.id:
        #     obj.updated_by = request.user
        obj.save()

class IssueAdmin(admin.ModelAdmin):
    inlines = [IssueDetailAdmin]
    list_display = ('issue_code', 'user', 'title', 'type', 'status', 'issue_date')
    list_filter = ('status', ('issue_date', DateRangeFilter))




admin.site.register(Issue, IssueAdmin)