from rest_framework import serializers
from issues.models import Issue, IssueDetail

class IssueDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = IssueDetail
        fields = ['message']

class IssueSerializer(serializers.ModelSerializer):
    issues = IssueDetailSerializer(many=True, required=False)
    class Meta:
        model = Issue
        fields = ['issue_code', 'user', 'issues', 'title', 'order_code', 'description', 'type', 'issue_date', 'status','image']

    def create(self, validated_data):

        issue = Issue.objects.create(**validated_data)

        IssueDetail.objects.create(issue=issue)

        return issue