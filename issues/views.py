from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from issues.serializers import IssueSerializer
from rest_framework.permissions import IsAuthenticated, AllowAny
from issues.models import Issue

class IssueAddAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        serializer = IssueSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'Issue Insert Successful',
                'data': [serializer.data]
            }
            return Response(response)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class IssueListAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request, pk):
        if self.request.user.id != int(pk):
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'You can not show this list!!'
            }
            return Response(response)
        issue = Issue.objects.filter(user=pk)
        serializer = IssueSerializer(issue,many=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Issue List',
            'data': serializer.data
        }
        return Response(response)