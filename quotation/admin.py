from django.contrib import admin
from quotation.models import Quotation, QuotationDetails
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter


class QutationDetailsAdmin(admin.StackedInline):
    model = QuotationDetails
    readonly_fields = ('message','unit_price', 'total_price', 'quantity','message_date')


class QuotationAdmin(admin.ModelAdmin):
    inlines = [QutationDetailsAdmin,]

    exclude = ['created_by', 'updated_by']
    list_display = ('quotation_code','product', 'phone', 'quantity', 'unit_price', 'total_price', 'created_at',)
    list_filter = ('is_accepted', ('created_at', DateRangeFilter))
    def save_model(self, request, obj, form, change):
        if not obj.id:
            obj.created_by = request.user
            obj.updated_by = request.user
        if change and obj.id:
            obj.updated_by = request.user
        obj.save()
admin.site.register(Quotation, QuotationAdmin)