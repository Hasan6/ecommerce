from django.urls import path

from quotation.views import *

app_name='quotation'

urlpatterns = [
    path('api/quote/create/', CreateQuoteView.as_view(), name='CreateQuote'),
    path('api/quote/details/<str:pk>/', QuotationDetailsView.as_view(), name='QuoteDetails'),
    path('api/quote/update/<str:pk>/', QuotationUpdateView.as_view(), name='UpdateQuote'),
    path('api/quote/user/wise/<str:pk>/', UserWiseQuotationView.as_view(), name='AllQuotes'),
    path('api/rfq/update/status/<str:pk>/', RfqUpdateView.as_view(), name='Rfq'),
]
