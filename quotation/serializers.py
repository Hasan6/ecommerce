from rest_framework import serializers

from quotation.models import *
from products.models import Product
from users.models import *

class QuoteDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuotationDetails
        fields = ['user','quantity', 'unit_price', 'total_price', 'message','message_date']


class RfqSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rfq
        fields = ['status']


class QuoteSerializer(serializers.ModelSerializer):
    quotation = QuoteDetailSerializer(many=True)
    rfq = RfqSerializer()

    class Meta:
        model = Quotation
        fields = ['id', 'rfq', 'quotation', 'product', 'status', 'buyer', 'email', 'phone', 'address', 'seller', 'unit_price', 'total_price', 'quantity', 'is_accepted', 'attachment1', 'attachment2']

    def create(self, validated_data):
        quote_details_data = validated_data.pop('quotation')
        rfq_satus_data = validated_data.pop('rfq')
        quote = Quotation.objects.create(**validated_data)

        Rfq.objects.create(quotation=quote, ** rfq_satus_data)

        for quote_details in quote_details_data:
            QuotationDetails.objects.create(quotation=quote, **quote_details)
        
        return quote

class QuoteUpdateSerializer(serializers.ModelSerializer):
    quotation = QuoteDetailSerializer(many=True, required=False)
    class Meta:
        model = Quotation
        fields = ['id', 'quotation', 'status', 'quantity', 'unit_price', 'total_price', 'attachment1', 'attachment2']

    def update(self, instance, validated_data):
        old_quantity = instance.quantity
        old_unit_price = instance.unit_price
        old_total_price = instance.total_price
        quotation_data = 0
        if 'quotation' in validated_data.keys():
            quotation_data = validated_data.pop('quotation')

        instance.status = validated_data.get('status',instance.status)
        instance.quantity = validated_data.get('quantity',instance.quantity)
        instance.unit_price = validated_data.get('unit_price',instance.unit_price)
        instance.total_price = validated_data.get('total_price',instance.total_price)
        instance.attachment1 = validated_data.get('attachment1',instance.attachment1)
        instance.attachment2 = validated_data.get('attachment2',instance.attachment2)

        if quotation_data is not 0:
            for quotation in quotation_data:
                quotation['quantity'] = old_quantity
                quotation['unit_price'] = old_unit_price
                quotation['total_price'] = old_total_price
                QuotationDetails.objects.create(**quotation, quotation=instance)

        instance.save()
        return instance

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'full_name']


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'category', 'brand', 'color', 'unit', 'seller', 'name', 'slug',
                  'description','unit_price', 'delivery_method', 'ddp_lead_time', 'ex_works_lead_time', 'commission',
                  'stock_quantity', 'status', 'created_at', 'image1', 'image2']


class RfqListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rfq
        fields = ['id','status']

class QuoteListSerializer(serializers.ModelSerializer):
    quotation = QuoteDetailSerializer(many=True)
    rfq = RfqSerializer()
    buyer = UserSerializer()
    seller = UserSerializer()

    class Meta:
        model = Quotation
        fields = ['id', 'rfq', 'quotation', 'product', 'status', 'buyer', 'email', 'phone', 'address', 'seller', 'unit_price', 'total_price', 'quantity', 'is_accepted', 'attachment1', 'attachment2', 'created_at']



class QuotationDetailsSerializer(serializers.ModelSerializer):
    quotation = QuoteDetailSerializer(many=True)
    product = ProductSerializer()
    buyer = UserSerializer()
    seller = UserSerializer()
    rfq = RfqListSerializer(many=True)
    class Meta:
        model = Quotation
        fields = ['id', 'rfq', 'quotation', 'product', 'buyer', 'email', 'phone', 'address', 'seller', 'unit_price', 'total_price', 'quantity',
                  'is_accepted', 'status', 'attachment1', 'attachment2', 'created_at']
