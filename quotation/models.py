from django.core.mail import send_mail
import random
import sys
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.db import models
from .tasks import quotation_confirmation_email
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from emdad.settings import EMAIL_HOST_USER

status_type = (
    ('1', 'Awaiting'),
    ('2', 'Sent'),
    ('3', 'Completed'),
    ('4', 'Closed'),
)



def random_string():
    return str(random.randint(10000, 99999))


class Quotation(models.Model):
    quotation_code = models.CharField(max_length=250, unique=True, default=random_string)
    product = models.ForeignKey(
        'products.Product',
        related_name="product",
        on_delete=models.CASCADE
    )
    buyer = models.ForeignKey(
        'users.User',
        related_name="buyer",
        on_delete=models.CASCADE
    )
    email = models.EmailField(max_length=254, unique=False)
    phone = models.CharField(max_length=25, blank=False)
    address = models.TextField(blank=True)
    seller = models.ForeignKey(
        'users.User',
        related_name="seller",
        on_delete=models.CASCADE
    )
    unit_price = models.DecimalField(max_digits=10, decimal_places=2,  blank=True, null=True)
    total_price = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    quantity = models.IntegerField()
    status = models.IntegerField(default=1)
    is_accepted = models.BooleanField(default=False)
    attachment1 = models.FileField(upload_to='uploads/quotation/attachment/', blank=True, null=True)
    attachment2 = models.FileField(upload_to='uploads/quotation/attachment/', blank=True, null= True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(
        'users.User',
        related_name="quotation_created_by",
        on_delete=models.CASCADE, blank=True, null=True
    )
    updated_by = models.ForeignKey(
        'users.User',
        related_name="quotation_updated_by",
        on_delete=models.CASCADE, blank=True, null=True
    )

    def __str__(self):
        return "%s" % self.quotation_code


class QuotationDetails(models.Model):
    quotation = models.ForeignKey(
        'Quotation',
        related_name="quotation",
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        'users.User',
        related_name="user",
        on_delete=models.CASCADE
    )
    unit_price = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    total_price = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    quantity = models.IntegerField(blank=True, null=True)
    message = models.TextField(blank=True)
    message_date = models.DateTimeField(auto_now_add=True)


class Rfq(models.Model):
    quotation = models.ForeignKey(
        'quotation.Quotation',
        related_name="rfq",
        on_delete=models.CASCADE
    )
    status = models.BooleanField(default=True)


@receiver(post_save, sender=Quotation)
def post_save_quotation_confirm(sender, instance, **kwargs):
    #quotation_confirmation_email.delay(buyer=instance.buyer.email, seller=instance.seller.email, product=instance.product.name)
    subject = 'Quotation Request!'
    html_message = render_to_string('mail_template/quotation_confirm.html', {'product': instance.product.name})
    plain_message = strip_tags(html_message)
    email_from = EMAIL_HOST_USER
    recipient_list = [instance.buyer.email, instance.seller.email]
    try:
        send_mail(subject, plain_message, email_from, recipient_list, html_message=html_message)
    except Exception as e:
        error = 'on line {}'.format(sys.exc_info()[-1].tb_lineno), e
        print(f"Error:{error}")