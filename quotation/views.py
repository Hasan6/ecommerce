from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.permissions import AllowAny
from rest_framework.generics import CreateAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework import status
import jwt
from rest_framework import permissions

from buyer.serializers import BuyerRegisterSerializer
from quotation.serializers import *
from users.models import *


class CreateQuoteView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        # email = request.data['email']
        # try:
        #     buyer_info = User.objects.get(email=email)
        # except:
        #     buyer_info = None
        #
        # serializer = ''
        #
        # if buyer_info:
        #     request.data['buyer'] = buyer_info.id
        #     serializer = QuoteSerializer(data=request.data)
        # # for unregistered user, register him/her
        # else:
        #
        #     email = request.data['email']
        #
        #     phone = request.data['phone']
        #     address = request.data['address']
        #     password = '1234!'
        #     data = {
        #         'email': email,
        #         'phone': phone,
        #         'password': password,
        #         'address': address,
        #     }
        #     buyer = BuyerRegisterSerializer(data=data)
        #     # print(buyer)
        #     if buyer.is_valid(raise_exception=True):
        #         buyer.save()
        #         # send email
        #         # first_name = validated_data['first_name']
        #         # email = validated_data['email']
        #         #
        #         # send_mail(
        #         #     'Welcome in Tmmim',
        #         #     f'Hello {first_name}, welcome in Tmmim. Your default passowrd is {password}',
        #         #     'haris.dipto@gmail.com',
        #         #     [email],
        #         #     fail_silently=False
        #         # )
        #         request.data['buyer'] = buyer.data.get('id')
        serializer = QuoteSerializer(data=request.data)
        # print(serializer)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            response = {
                'success': 'True',
                'status_code': status.HTTP_200_OK,
                'message': 'Quote created successfully',
                'data': serializer.data
            }

            return Response(response, status=status.HTTP_200_OK)


class UserWiseQuotationView(APIView):
    permission_classes = [IsAuthenticated]

    # for all quotes
    def get(self, request, pk):
        user = User.objects.get(id=pk)
        group = user.groups.all()
        if str(group[0]) == 'seller':
            if self.request.user.id != int(pk):
                response = {
                    'success': 'True',
                    'status code': status.HTTP_200_OK,
                    'message': 'You can not show this list!!'
                }
                return Response(response)
            quotations = Quotation.objects.filter(seller=pk)
        elif str(group[0]) == 'buyer':
            if self.request.user.id != int(pk):
                response = {
                    'success': 'True',
                    'status code': status.HTTP_200_OK,
                    'message': 'You can not show this list!!'
                }
                return Response(response)
            quotations = Quotation.objects.filter(buyer=pk)

        serializer = QuoteListSerializer(quotations, many=True)


        response = {
            'success': 'True',
            'status_code': status.HTTP_200_OK,
            'message': 'All quotations',
            'data': serializer.data
        }


        return Response(response, status=status.HTTP_200_OK)

class QuotationDetailsView(APIView):
    permission_classes = [IsAuthenticated]

    # for all quotes
    def get(self, request, pk):
        quotations = Quotation.objects.get(id=pk)
        serializer = QuotationDetailsSerializer(quotations)


        response = {
            'success': 'True',
            'status_code': status.HTTP_200_OK,
            'message': 'Quotations Details',
            'data': serializer.data
        }


        return Response(response, status=status.HTTP_200_OK)

class QuotationUpdateView(APIView):
    permission_classes = [IsAuthenticated]

    def put(self, request, pk, *args, **kwargs):
        rfq_info = Rfq.objects.get(quotation=pk)
        if rfq_info.status == 1:
            quotation = Quotation.objects.get(id=pk)
            # request.data['is_accepted'] = 0
            # print("riey")
            serializer = QuoteUpdateSerializer(quotation, data=request.data)

            if serializer.is_valid(raise_exception=True):
                serializer.save()
                response = {
                    'success': 'True',
                    'status_code': status.HTTP_200_OK,
                    'message': 'Quote update successfully',
                    'data': serializer.data
                }

                return Response(response, status=status.HTTP_200_OK)
        else:
            response = {
                'success': 'True',
                'status_code': status.HTTP_200_OK,
                'message': 'RFQ Closed'
            }

            return Response(response)

class RfqUpdateView(APIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, pk):
        rfq_info = Rfq.objects.get(id=pk)
        serializer = RfqListSerializer(rfq_info,data=request.data)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            response = {
                'success': 'True',
                'status_code': status.HTTP_200_OK,
                'message': 'RFQ Update successfully',
                'data': serializer.data
            }

            return Response(response, status=status.HTTP_200_OK)