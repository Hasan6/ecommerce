from django.shortcuts import render, get_object_or_404
from subscriptions.serializers import SubscriptionPlanSerializer, SubscriptionSerializer, SubscriptionDetailsSerilizer
from subscriptions.models import SubscriptionPlan, Subscription
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.decorators import login_required
from rest_framework.permissions import IsAuthenticated, AllowAny
from users.models import User
from dateutil.relativedelta import *
from datetime import date



# Create your views here.

# @login_required()
class SubcriptionPlanAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request):
        plan = SubscriptionPlan.objects.all()
        serializer = SubscriptionPlanSerializer(plan,many=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Subscription Plan List',
            'data': [serializer.data]
        }
        return Response(response)
class SubscriptionHistoryAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request,pk):
        if self.request.user.id != int(pk):
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'You can not show this list!!'
            }
            return Response(response)
        subsc = Subscription.objects.filter(seller=pk,is_deleted=0).order_by('-id')
        serializer = SubscriptionDetailsSerilizer(subsc, many=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Subscription History',
            'data': [serializer.data]
        }
        return Response(response)

class SubscriptionDetailsAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request,pk):
        tot_subs = Subscription.objects.filter(seller=pk).count()
        if tot_subs > 1:
            subscription_detail = Subscription.objects.filter(seller=pk).order_by('-id')[0]
        else:
            subscription_detail = Subscription.objects.get(seller=pk)

        serializer = SubscriptionDetailsSerilizer(subscription_detail)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Subscription Details',
            'data': [serializer.data]
        }
        return Response(response)
# @login_required()
class SubcriptionAPIView(APIView):
    permission_classes = (AllowAny,)

    def post(self,request):
        seller_id = request.data.get('seller')
        User.objects.filter(id=seller_id).update(is_subscribed=1)

        subscription_plan = request.data.get('subscription_plan')

        subscription_plan_info = SubscriptionPlan.objects.get(id=subscription_plan)
        subcription_plan_type = subscription_plan_info.subscription_plan

        today = date.today()

        if subcription_plan_type == 'h':
            expireDate = today+relativedelta(months=+6)
        elif subcription_plan_type == 'y':
            expireDate = today+relativedelta(years=+1)


        request.data['end_date'] = expireDate
        request.data['start_date'] = today
        request.data['subscription_type'] = 0

        serializer = SubscriptionSerializer(data=request.data)

        if serializer.is_valid():
            # serializer.data.vat_amount = vat_amount
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class RenewSubscriptionAPIView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        seller_id = request.data.get('seller')
        User.objects.filter(id=seller_id).update(is_subscribed=1)



        subscription_plan = request.data.get('subscription_plan')

        subscription_plan_info = SubscriptionPlan.objects.get(id=subscription_plan)
        subcription_plan_type = subscription_plan_info.subscription_plan

        today = date.now()

        if subcription_plan_type == 'h':
            expireDate = today + relativedelta(months=+6)
        elif subcription_plan_type == 'y':
            expireDate = today + relativedelta(years=+1)


        request.data['end_date'] = expireDate
        request.data['start_date'] = today
        request.data['subscription_type'] = 1

        serializer = SubscriptionSerializer(data=request.data)

        if serializer.is_valid():
            # serializer.data.vat_amount = vat_amount
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CancelSubscriptionAPIView(APIView):
    permission_classes = (AllowAny,)
    def GetSubscriptionById(self,pk):
        try:
            model = Subscription.objects.get(id=pk)
            return model
        except Subscription.DoesNotExist:
            return

    def delete(self,request,pk):
        if not self.GetSubscriptionById(pk):
            return Response(f'Subcription {pk} is Not Found', status=status.HTTP_404_NOT_FOUND)
        if self.GetSubscriptionById(pk):
            Subscription.objects.filter(id=pk).update(is_deleted=1)
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'Subscription Canceled'
            }
            return Response(response)