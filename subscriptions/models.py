import sys

from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from .tasks import subscription_confirmation_email
# Create your models here.
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from emdad.settings import EMAIL_HOST_USER

plan = (
        ("y", 'Annually'),
        ("h", 'Semi-Annually'),
    )
cardType = (
        (0, 'Wire Transfer'),
        (1, 'Card'),
    )
statusType = (
        (1, 'Paid'),
        (0, 'Unpaid'),
    )
subscriptionType = (
        (0, 'New'),
        (1, 'Renew'),
    )

class SubscriptionPlan(models.Model):
    title = models.CharField(max_length=255, null=True, blank=True)
    fees = models.DecimalField(max_digits=10, decimal_places=2)
    subscription_plan = models.CharField(max_length=1, choices=plan)
    description = models.TextField(blank=True, null=True)

    def __str__(self):
        if self.subscription_plan == 'y':
            return "Annually"+' - '+str(self.fees)
        else:
            return "Semi-Annually" + ' - ' + str(self.fees)


class Subscription(models.Model):
    seller = models.ForeignKey(
        'users.User',
        related_name="subscriptions",
        on_delete=models.CASCADE,
        verbose_name='Seller'
    )
    subscription_plan = models.ForeignKey(
        'SubscriptionPlan',
        related_name="subscriptions_plan",
        on_delete=models.CASCADE,
        verbose_name='Subscription Plan'
    )
    fees = models.DecimalField(max_digits=10, decimal_places=2, blank=True,null=True)
    payment_type = models.IntegerField(choices=cardType, default=1, blank=True)
    payment_status = models.BooleanField(default=False, choices=statusType, blank=True, null=True, verbose_name='Payment Status')
    total_paid_amount = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    discount_coupon = models.ForeignKey(
        'coupons.Coupon',
        related_name="coupons",
        on_delete=models.CASCADE, blank=True,
        verbose_name='Coupon',
        null=True
    )
    discount_amount = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    vat_amount = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)
    start_date = models.DateField(blank=True, null=True)
    end_date = models.DateField(blank=True, null=True)
    subscription_type = models.IntegerField(choices=subscriptionType, default=0, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(
        'users.User',
        related_name="subscriptions_created_by",
        on_delete=models.CASCADE
    )
    updated_at = models.DateTimeField(auto_now=True)
    updated_by = models.ForeignKey(
        'users.User',
        related_name="subscriptions_updated_by",
        on_delete=models.CASCADE, blank=True, null=True
    )
    is_deleted = models.BooleanField(default=False, verbose_name='Archived')


@receiver(post_save, sender=Subscription)
def post_save_subscriptions_confirm(sender, instance, **kwargs):
    subscription_confirmation_email.delay(email=instance.seller.email)