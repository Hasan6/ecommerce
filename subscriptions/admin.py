import sys

from django.contrib import admin
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from emdad.settings import EMAIL_HOST_USER
from subscriptions.models import SubscriptionPlan, Subscription
from django_summernote.admin import SummernoteModelAdmin

class SubscriptionPlanAdmin(SummernoteModelAdmin):
    list_display = ('title', 'subscription_plan', 'fees', )
    summernote_fields = ('description',)

admin.site.register(SubscriptionPlan, SubscriptionPlanAdmin)


class SubscriptionAdmin(admin.ModelAdmin):
    exclude = ['created_by','updated_by']
    list_display = ('seller', 'payment_status', 'total_paid_amount', 'discount_amount', 'start_date', 'end_date', 'created_at')

    def save_model(self, request, obj, form, change):
        if not obj.id:
            obj.created_by = request.user
            obj.updated_by = request.user
        if change and obj.id:
            obj.updated_by = request.user
        obj.save()

    class Media:
        js = (
            '//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js',  # jquery
            'custom.js',  # app static folder
        )


admin.site.register(Subscription, SubscriptionAdmin)