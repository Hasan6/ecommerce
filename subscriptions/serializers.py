from rest_framework import serializers
from subscriptions.models import Subscription, SubscriptionPlan
from users.models import User
from coupons.models import Coupon

class SubscriptionPlanSerializer(serializers.ModelSerializer):
    class Meta:
        model = SubscriptionPlan
        fields = '__all__'

class SubscriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subscription

        fields = ['id', 'fees','payment_type','payment_status','total_paid_amount','discount_amount','vat_amount','start_date','end_date', 'seller','subscription_plan','discount_coupon', 'vat_amount', 'subscription_type', 'created_by']



class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'full_name']
class CouponSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coupon
        fields = ['id', 'coupon_title', 'coupon_code']

class SubscriptionDetailsSerilizer(serializers.ModelSerializer):
    seller = UserSerializer()
    discount_coupon = CouponSerializer()
    class Meta:
        model = Subscription
        fields = ['id', 'fees','payment_type','payment_status','total_paid_amount','discount_amount','vat_amount','start_date','end_date', 'seller','subscription_plan','discount_coupon', 'subscription_type',]