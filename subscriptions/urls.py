from django.urls import path
from .views import SubcriptionPlanAPIView, SubcriptionAPIView, RenewSubscriptionAPIView, CancelSubscriptionAPIView, SubscriptionDetailsAPIView, SubscriptionHistoryAPIView

urlpatterns = [
    # list of subscription plan
    path('api/subscription/plans/', SubcriptionPlanAPIView.as_view()),
    # subscribed seller list not needed instead just keep the subscribe functionality
    path('api/subscribe/', SubcriptionAPIView.as_view()),
    # renew subscriptin for seller
    path('api/renew/subscription/', RenewSubscriptionAPIView.as_view()),
    path('api/subscription/details/<str:pk>/', SubscriptionDetailsAPIView.as_view()),
    # cancel subscription
    path('api/cancel/subscription/<str:pk>/', CancelSubscriptionAPIView.as_view()),
    # subscription history
    path('api/subscription/history/<str:pk>/', SubscriptionHistoryAPIView.as_view()),
]