from django.contrib import admin
from coupons.models import Coupon
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
# Register your models here.


class CouponAdmin(admin.ModelAdmin):
    exclude = ['created_by', 'updated_by']
    list_display = ('coupon_title', 'coupon_code', 'coupon_section', 'discount_amount', 'is_active', )

    actions = ('set_coupons_to_published',)

    def set_coupons_to_published(self, request, queryset):
        count = queryset.update(is_active=True)
        self.message_user(request, '{} coupons have been published.'.format(count))

    set_coupons_to_published.short_description = 'Mark selected coupons as published'

    list_filter = ('is_active', 'coupon_section', ('start_datetime', DateTimeRangeFilter))
    search_fields = ('coupon_title', 'coupon_code', )

    def save_model(self, request, obj, form, change):
        if not obj.id:
            obj.created_by = request.user
            obj.updated_by = request.user
        if change and obj.id:
            obj.updated_by = request.user
        obj.save()


admin.site.register(Coupon, CouponAdmin)