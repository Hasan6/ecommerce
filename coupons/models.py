from django.db import models

# Create your models here.

section = (
        (1, 'Subscription'),
        (2, 'Order'),
    )

type = (
        (1, 'Flat'),
        (2, 'Percentage'),
    )


class Coupon(models.Model):
    coupon_title = models.CharField(max_length=250, unique=True)
    coupon_code = models.CharField(max_length=250, unique=True)
    discount_amount = models.DecimalField(max_digits=10, decimal_places=2)
    coupon_type = models.IntegerField(choices=type, default=1)
    coupon_section = models.IntegerField(choices=section, default=2)
    start_datetime = models.DateTimeField()
    end_datetime = models.DateTimeField()
    created_by = models.ForeignKey(
        'users.User',
        related_name="coupon_created_by",
        on_delete=models.CASCADE, blank=True
    )
    updated_by = models.ForeignKey(
        'users.User',
        related_name="coupon_updated_by",
        on_delete=models.CASCADE, blank=True
    )
    is_active = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.coupon_title
