from django.urls import path
from .views import CouponListAPIView, CouponCodeAmountAPIView

urlpatterns = [
    path('api/coupon/list', CouponListAPIView.as_view()),
    path('api/coupon/code/amount', CouponCodeAmountAPIView.as_view()),
]