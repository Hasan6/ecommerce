from rest_framework import serializers
from coupons.models import Coupon

class CouponSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coupon
        fields = ['id','coupon_title','discount_amount','start_datetime','end_datetime','coupon_type']