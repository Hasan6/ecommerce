from django.shortcuts import render
from coupons.serializers import CouponSerializer
from coupons.models import Coupon
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth.decorators import login_required
from rest_framework.permissions import IsAuthenticated, AllowAny

class CouponListAPIView(APIView):
    permission_classes = (AllowAny,)
    def get(self,request):
        plan = Coupon.objects.all()
        serializer = CouponSerializer(plan,many=True)
        return Response(serializer.data)

class CouponCodeAmountAPIView(APIView):
    permission_classes = (IsAuthenticated,)
    def post(self, request):
        # request.data.get('id')
        coupon_section = request.data.get('coupon_section')
        coupon_code = request.data.get('coupon_code')

        try:
            coupon_info = Coupon.objects.get(coupon_code=coupon_code,coupon_section=coupon_section,is_active=1)
        except Coupon.DoesNotExist:
            response = {
                'success': 'True',
                'status code': status.HTTP_404_NOT_FOUND,
                'message': 'Coupon Code ' + coupon_code + ' is Not Found'
            }
            return Response(response)
        serializer = CouponSerializer(coupon_info)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Coupon',
            'data': [serializer.data]
        }
        return Response(response)