$(function(){
    /* this code runs after page load */
     $("#id_fees").attr('readonly','readonly');
    $("#id_subscription_plan").change(function(){
        var fees = $( "#id_subscription_plan option:selected" ).text().split("-").pop();
        $("#id_fees").val(parseInt(fees));
        $("#id_fees").attr('readonly','readonly');

    });
});

