from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from buyer.serializers import BuyerRegisterSerializer
from rest_framework.permissions import AllowAny
# Create your views here.


def index(request):
    return render(request, 'index.html', {})


class BuyerRegisterAPIView(APIView):
    serializer_class = BuyerRegisterSerializer
    permission_classes = (AllowAny,)

    def post(self, request):
        serializer = self.serializer_class(data=request.data)

        # print(serializer)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'User registered  successfully',
            'data': []
        }
        status_code = status.HTTP_200_OK
        return Response(response, status=status_code)