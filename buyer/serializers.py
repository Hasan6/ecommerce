from rest_framework import serializers
from django.contrib.auth.models import User, Group
from users.models import User


class BuyerRegisterSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'full_name', 'password', 'email', 'phone')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        my_group = Group.objects.get(name='buyer')
        user = User.objects.create_user(**validated_data)
        my_group.user_set.add(user)
        return user
