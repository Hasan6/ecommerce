from django.urls import path
from . import views
from .views import BuyerRegisterAPIView

urlpatterns = [
    path('', views.index, name="buyer.index"),
    path('api/buyer/registration/', BuyerRegisterAPIView.as_view(), name="Buyer_reg"),
]