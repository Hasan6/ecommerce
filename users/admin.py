from users.forms import UserChangeForm, UserCreationForm
from users.models import City, User, Country
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from rangefilter.filter import DateRangeFilter
from django.utils.translation import ugettext_lazy as _


class UsersAdmin(UserAdmin):
    # The forms to add and change user instances
    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference the removed 'username' field
    form = UserChangeForm
    add_form = UserCreationForm
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Profile info'), {'fields': ('full_name', 'phone', 'gender', 'dob', 'profile_pic', 'city',
                                        'address',
                                        )}),
        (_('Permissions'),
         {'fields': ('groups', 'is_approved', 'is_subscribed', 'is_active', 'is_staff', 'is_superuser',)}),
        (_('Store info'), {'fields': ('store_name', 'store_pic', 'store_address')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
         ),
    )

    list_display = (
        'email',  'gender', 'is_staff', 'is_approved', 'is_subscribed', 'date_joined',
        )
    list_display_links = ('email',)
    list_filter = ['groups', 'gender', 'is_approved', 'is_subscribed', ('date_joined', DateRangeFilter)]
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('email',)
    readonly_fields = ('date_joined', 'last_login')


admin.site.register(User, UsersAdmin)
admin.site.register(Country)
admin.site.register(City)
