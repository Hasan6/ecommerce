import sys
import uuid

from django.db import models
# from django.utils import timezone
# from django.utils.http import urlquote
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from emdad.settings import EMAIL_HOST_USER
from users.config import GenderType, UserType
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from .tasks import post_save_user_registration_confirm, send_password_reset_email
# from users.managers import UserManager, PasswordResetManager
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.dispatch import receiver
from django.urls import reverse
from django_rest_passwordreset.signals import reset_password_token_created
from django.core.mail import send_mail
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
# Create your models here.
from users.managers import UserManager
from django.template.loader import render_to_string
from django.utils.html import strip_tags

class Country(models.Model):
    name = models.CharField(max_length=100, blank=True)
    iso3 = models.CharField(max_length=3, blank=True)
    iso2 = models.CharField(max_length=2, blank=True)
    phone_code = models.CharField(max_length=100, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Country')
        verbose_name_plural = _('Countries')

    def __str__(self):
        return "%s" % self.name


class City(models.Model):
    name = models.CharField(max_length=100, blank=True)
    country = models.ForeignKey(Country, blank=True, null=True, on_delete=models.PROTECT)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.name


class User(AbstractBaseUser, PermissionsMixin):
    username = None
    email = models.EmailField(_('email address'), max_length=254, unique=True)
    full_name = models.CharField(_('full name'), max_length=30, blank=True)
    phone = models.CharField(_('phone'), max_length=25, blank=True)
    gender = models.CharField(max_length=1, choices=GenderType.CHOICES, blank=True)
    dob = models.DateField(blank=True, null=True)
    country = models.ForeignKey(Country, blank=True, null=True, on_delete=models.PROTECT)
    city = models.ForeignKey(City, blank=True, null=True, on_delete=models.PROTECT)
    address = models.TextField(blank=True)
    zip_code = models.PositiveSmallIntegerField(blank=True, null=True)
    profile_pic = models.ImageField(upload_to='uploads/users/images/', blank=True, null=True)
    store_name = models.CharField(max_length=100, blank=True)
    store_address = models.CharField(max_length=100, blank=True)
    store_pic = models.ImageField(upload_to='uploads/store/images/', blank=True, null=True)
    verify_token = models.UUIDField(editable=False,default=uuid.uuid4().hex, blank=True, null=True)
    is_approved = models.BooleanField(_('approved'), default=False,
                                      help_text=_('Designates whether this user is verified'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as active. '
                                                'Unselect this instead of deleting accounts.'))
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin site.'))
    is_subscribed = models.BooleanField(_('subscribed'), default=False,
                                        help_text=_('Designates whether this user\'s store is subscribed'))
    is_deleted = models.BooleanField(_('deleted'), default=False,
                                     help_text=_('Designates whether this user is deleted'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)

    FIELDS_TO_CHECK = ['is_subscribed']

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    class Meta:
        db_table = 'auth_user'
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.email


@receiver(post_save, sender=User)
def post_save_registration_confirm(sender, instance, created, **kwargs):
    if created:
        subject = 'Registration Verify Token!'
        context = {'token':'http://127.0.0.1:4200/registration/profile/verify/'+urlsafe_base64_encode(force_bytes(instance.verify_token)), 'date': instance.date_joined.strftime("%m-%d-%Y %I:%M:%S %p")}
        html_message = render_to_string('mail_template/registration_verify.html', context )
        plain_message = strip_tags(html_message)
        email_from = EMAIL_HOST_USER
        recipient_list = [instance.email, ]
        try:
            send_mail(subject, plain_message, email_from, recipient_list, html_message=html_message)
        except Exception as e:
            error = 'on line {}'.format(sys.exc_info()[-1].tb_lineno), e
            print(f"Error:{error}")
        #post_save_user_registration_confirm.delay(email=instance.email, date=instance.date_joined)


@receiver(reset_password_token_created)
def password_reset_token_created(sender, instance, reset_password_token, *args, **kwargs):
    subject = 'Emdad : Password Reset Token!'
    html_message = render_to_string('mail_template/password_reset.html', {'token': reset_password_token.key})
    plain_message = strip_tags(html_message)
    email_from = EMAIL_HOST_USER
    recipient_list = [reset_password_token.user.email, ]
    try:
        send_mail(subject, plain_message, email_from, recipient_list, html_message=html_message)
    except Exception as e:
        error = 'on line {}'.format(sys.exc_info()[-1].tb_lineno), e
        print(f"Error:{error}")
    # email_plaintext_message = "{}?token={}".format(reverse('password_reset:reset-password-request'), reset_password_token.key)
    #send_password_reset_email.delay(email=reset_password_token.user.email, token=reset_password_token.key)
    # send_mail(
    #     # title:
    #     "Password Reset for {title}".format(title="Some website title"),
    #     # message:
    #     email_plaintext_message,
    #     # from:
    #     EMAIL_HOST_USER,
    #     # to:
    #     [reset_password_token.user.email]
    # )