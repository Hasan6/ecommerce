from rest_framework.generics import RetrieveAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from users.serializers import CountrySerializer, CitySerializer, BuyerProfileSerializer, ChangePasswordSerializer, \
    SellerProfileSerializer, LoginSerializer, CityDetailSerializer
from users.models import Country, User, City
from orders.models import AddressBook
from rest_framework import permissions
from rest_framework.permissions import IsAuthenticated
# from django.contrib.auth.models import User
from rest_framework import generics
from django.core.exceptions import ObjectDoesNotExist
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.permissions import AllowAny
from django.contrib.auth import logout
from django.shortcuts import get_object_or_404
from django.utils.http import urlsafe_base64_decode
from django.utils.encoding import force_text


class Logout(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, format=None):
        # using Django logout
        logout(request)
        content = {
            'success': True,
            'status code': status.HTTP_201_CREATED,
            'message': 'logout Successfully',
        }
        return Response(content)


class Login(RetrieveAPIView):
    permission_classes = (AllowAny,)
    serializer_class = LoginSerializer

    def post(self, request):
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'User logged in  successfully',
            # 'exp': serializer.data['token'],
        }
        status_code = status.HTTP_200_OK
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            if serializer.data['group'] == request.data.get('group'):
                user_data = User.objects.get(email=request.data.get('email'))
                # print(user_data)
                response['token'] = serializer.data['token']
                response['group'] = serializer.data['group']
                response['full_name'] = user_data.full_name
            else:
                response['success'] = 'False'
                response['status code'] = status.HTTP_400_BAD_REQUEST
                response['message'] = 'Forbidden for this user!!'
                status_code = status.HTTP_400_BAD_REQUEST
            return Response(response, status=status_code)
        response['success'] = 'False'
        response['status code'] = status.HTTP_400_BAD_REQUEST
        response['message'] = 'User Not found'
        return Response(response, status=status_code)


# # ProfileList is not needed
# class BuyerProfileList(APIView):
#     # permission_classes = (IsAuthenticated, )
#
#     def get(self, request):
#         response = {
#             'success': 'True',
#             'status code': status.HTTP_200_OK,
#             'message': 'Buyer List',
#             'data': []
#         }
#         profile_list = UserProfile.objects.filter(user_type="BUYER")
#         serializer = BuyerProfileSerializer(profile_list, many=True)
#         response['data'] = serializer.data
#         return Response(response)


class ProfileDetail(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk):
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'User found successfully',
            'data': []
        }
        try:
            user = User.objects.get(id=pk)
        except ObjectDoesNotExist:
            response['message'] = 'User not found'
            return Response(response, status=status.HTTP_404_NOT_FOUND)
        serializer = SellerProfileSerializer(user, many=False)
        response['data'] = serializer.data
        return Response(response, status=status.HTTP_200_OK)


# # ProfileCreate is not needed
# class ProfileCreate(APIView):
#
#     def post(self, request):
#         serializer = BuyerProfileSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class BuyerProfileUpdate(APIView):
    permission_classes = (IsAuthenticated, )
    authentication_class = JSONWebTokenAuthentication

    def post(self, request, pk):
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Profile Updated',
            'data': []
        }
        if self.request.user.id != int(pk):
            response['status code'] = status.HTTP_400_BAD_REQUEST
            response['message'] = 'You can not update this profile!!'
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        user = User.objects.get(id=pk)
        id = user.id
        serializer = BuyerProfileSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            response['data'] = serializer.data
            # address book create
            if 'city' in request.data:
                city = request.data['city']
                city_info = City.objects.get(id=city)
                country = city_info.country_id
                address = request.data['address']
                zip_code = request.data['zip_code']
                default = 1
                existinfo = AddressBook.objects.filter(buyer_id=id)
                if existinfo:
                    AddressBook.objects.filter(buyer_id=id,is_default=1).update(buyer_id=id,city_id=city,country_id=country,zip_code=zip_code,address=address,is_default=default)
                else:
                    addr = AddressBook(buyer_id=id,city_id=city,country_id=country,zip_code=zip_code,address=address,is_default=default)
                    addr.save()
            return Response(response, status=status.HTTP_201_CREATED)
        response['message'] = 'Something went wrong'
        response['data'] = serializer.errors
        return Response(response, status=status.HTTP_400_BAD_REQUEST)


# # ProfileDelete is not needed
# class ProfileDelete(APIView):
#
#     def delete(self, request, pk):
#         user_profile = UserProfile.objects.get(id=pk)
#         if not user_profile:
#             return Response(status=status.HTTP_404_NOT_FOUND)
#         user_profile.delete()
#         return Response("Profile deleted!!")
# class SellerProfileList(APIView):
#     # permission_classes = (IsAuthenticated, )
#
#     def get(self, request):
#         response = {
#             'success': 'True',
#             'status code': status.HTTP_200_OK,
#             'message': 'Seller List',
#             'data': []
#         }
#         profile_list = User.objects.filter(user_type="SELLER")
#         serializer = SellerProfileSerializer(profile_list, many=True)
#         response['data'] = serializer.data
#         return Response(response)


class SellerProfileUpdate(APIView):
    permission_classes = (IsAuthenticated,)
    authentication_class = JSONWebTokenAuthentication

    def post(self, request, pk):
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Profile Updated',
            'data': []
        }
        if self.request.user.id != int(pk):
            response['status code'] = status.HTTP_400_BAD_REQUEST
            response['message'] = 'You can not update this profile!!'
            return Response(response, status=status.HTTP_400_BAD_REQUEST)
        user = User.objects.get(id=pk)
        serializer = SellerProfileSerializer(user, data=request.data)
        if serializer.is_valid():
            serializer.save()
            response['data'] = serializer.data
            return Response(response, status=status.HTTP_201_CREATED)
        response['message'] = 'Something went wrong'
        response['data'] = serializer.errors
        return Response(response, status=status.HTTP_400_BAD_REQUEST)


class CountryList(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Country List',
            'data': []
        }
        country_list = Country.objects.all()
        serializer = CitySerializer(country_list, many=True)
        response['data'] = serializer.data
        return Response(response)


class CountryAdd(APIView):

    def post(self, request):
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Country Added Successfully',
            'data': []
        }
        serializer = CountrySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            response['data'] = serializer.data
            return Response(response, status=status.HTTP_201_CREATED)
        response['message'] = 'Something went wrong'
        response['data'] = serializer.errors
        return Response(response, status=status.HTTP_400_BAD_REQUEST)


class CityList(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, pk):
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'City List',
            'data': []
        }
        city_list = City.objects.filter(country_id=pk)
        serializer = CitySerializer(city_list, many=True)
        response['data'] = serializer.data
        return Response(response)


class CityDetails(APIView):
    permission_classes = (AllowAny,)

    def get(self, request, pk):
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'City Details',
            'data': []
        }
        city = City.objects.get(id=pk)
        serializer = CityDetailSerializer(city)
        response['data'] = serializer.data
        return Response(response)


class CityAdd(APIView):

    def post(self, request):
        serializer = CitySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserEmailVerification(APIView):
    permission_classes = (AllowAny,)
    def get(self, request, token):
        decode_token = force_text(urlsafe_base64_decode(token))
        # print(decode_token)
        user_info = get_object_or_404(User, verify_token=decode_token)
        if user_info:
            exit_check = User.objects.filter(is_approved=1, verify_token=decode_token)[:1]
            if exit_check:
                response = {
                    'success': 'True',
                    'status code': status.HTTP_200_OK,
                    'detail': 'User already approved'
                }
                return Response(response)
            else:
                User.objects.filter(id=user_info.id).update(is_approved=1)
                response = {
                    'success': 'True',
                    'status code': status.HTTP_200_OK,
                    'detail': 'User has been approved'
                }
                return Response(response)
        else:
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'User not found'
            }
            return Response(response)


class ChangePassword(generics.UpdateAPIView):
    serializer_class = ChangePasswordSerializer
    model = User
    permission_classes = (IsAuthenticated,)

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def update(self, request, *args, **kwargs):
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            # Check old password
            if not self.object.check_password(serializer.data.get("old_password")):
                return Response({"old_password": ["Wrong password."]}, status=status.HTTP_400_BAD_REQUEST)
            # set_password also hashes the password that the user will get
            self.object.set_password(serializer.data.get("new_password"))
            self.object.save()
            response = {
                'status': 'success',
                'code': status.HTTP_200_OK,
                'message': 'Password updated successfully',
                'data': []
            }

            return Response(response)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)