import sys

from celery import shared_task
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from emdad.celery import app
from emdad.settings import EMAIL_HOST_USER


@app.task
def post_save_user_registration_confirm(email, date):
    subject = 'Registration Confirmed!'
    html_message = render_to_string('mail_template/registration_confirm.html', {'date': str(date)})
    plain_message = strip_tags(html_message)
    email_from = EMAIL_HOST_USER
    recipient_list = [email, ]
    try:
        send_mail(subject, plain_message, email_from, recipient_list, html_message=html_message)
    except Exception as e:
        error = 'on line {}'.format(sys.exc_info()[-1].tb_lineno), e
        print(f"Error:{error}")


@app.task
def send_password_reset_email(email, token):
    subject = 'Emdad : Password Reset Token!'
    html_message = render_to_string('mail_template/password_reset.html', {'token': token})
    plain_message = strip_tags(html_message)
    email_from = EMAIL_HOST_USER
    recipient_list = [email, ]
    try:
        send_mail(subject, plain_message, email_from, recipient_list, html_message=html_message)
    except Exception as e:
        error = 'on line {}'.format(sys.exc_info()[-1].tb_lineno), e
        print(f"Error:{error}")