from django.conf import settings
from django.conf.urls import url
from django.urls import path
from django.views.static import serve
from .views import ProfileDetail, BuyerProfileUpdate, CountryList, ChangePassword, \
    CityList, SellerProfileUpdate, Logout, Login, CityDetails, UserEmailVerification

# from . import views

urlpatterns = [
    path('api/logout/', Logout.as_view(), name='Logout'),
    path('api/login/', Login.as_view(), name='Login'),
    path('api/profile/details/<str:pk>/', ProfileDetail.as_view(), name="Profile_details"),
    # buyer api
    path('api/buyer/profile/update/<str:pk>/', BuyerProfileUpdate.as_view(), name="Buyer_Profile_update"),
    path('api/registration/profile/verify/<str:token>/', UserEmailVerification.as_view(), name="email_verify"),
    # seller api
    path('api/seller/profile/update/<str:pk>/', SellerProfileUpdate.as_view(), name="Seller_Profile_update"),
    # change password
    path('api/change/password/', ChangePassword.as_view(), name="Change_password"),
    # country api
    path('api/country/list/', CountryList.as_view(), name="Country_list"),
    # city api
    path('api/city/list/<str:pk>/', CityList.as_view(), name="City_list"),
    path('api/city/details/<str:pk>/', CityDetails.as_view(), name="City_details"),
    url(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT,})
    # path('', views.authlogin, name="login"),
    # path("login_check", views.login_check, name="login_check"),
    # path("logout", views.authlogout, name="logout"),
]