from django.urls import path
from .views import ProductListAPIView, ProductDetailsAPIView, ProductInsertAPIView, DeleteProductAPIView, \
    UpdateProductAPIView, ProductBySellerAPIView, CategoryWiseProductAPIView, ProductFilteringAPIView, BulkUploadView, \
    PickupAddressView, CommentProductAPIView, CommentReplyAPIView, ProductCommentListAPIView, WishlistAPIView, WishlistAddAPIView, ProductFilterListAPIView, SimilarProductsListAPIView, SellerCategoryStoreInfoAPIView, PopularProductListAPIView, WishlistRemoveAPIView, ProductOrderCheckAPIView, WishlistStatusAPIView, PopularCategoryListAPIView

urlpatterns = [
    path('api/product/list/', ProductListAPIView.as_view()),
    path('api/product/order/check/<str:pk>/', ProductOrderCheckAPIView.as_view()),
    path('api/product/popular/products/', PopularProductListAPIView.as_view()),
    path('api/product/popular/category/', PopularCategoryListAPIView.as_view()),
    path('api/seller/category/store/info/<str:pk>/', SellerCategoryStoreInfoAPIView.as_view()),
    path('api/product/seller/products/<str:pk>/', ProductBySellerAPIView.as_view()),
    path('api/product/details/<str:pk>/', ProductDetailsAPIView.as_view()),
    path('api/product/add/', ProductInsertAPIView.as_view()),
    path('api/product/update/<str:pk>/', UpdateProductAPIView.as_view()),
    path('api/product/delete/<str:pk>/', DeleteProductAPIView.as_view()),
    path('api/product/category/products/<str:pk>/', CategoryWiseProductAPIView.as_view()),
    path('api/product/similar/products/<str:pk>/', SimilarProductsListAPIView.as_view()),
    path('api/product/filter/<str:query>/', ProductFilteringAPIView.as_view()),
    path('api/product/filter/list', ProductFilterListAPIView.as_view()),
    # Bulk Product Upload
    path('api/product/bulk/add/', BulkUploadView.as_view()),
    # Pickup address by product Id
    path('api/pickup/<str:pk>/', PickupAddressView.as_view()),
    # comments entry
    path('api/product/comment/list/<str:pk>/', ProductCommentListAPIView.as_view()),
    path('api/product/comment/add/', CommentProductAPIView.as_view()),
    path('api/product/comment/reply/add/', CommentReplyAPIView.as_view()),

    # wishlist
    path('api/product/wishlist/status/check/<str:pk>/', WishlistStatusAPIView.as_view()),
    path('api/product/wishlist/add/', WishlistAddAPIView.as_view()),
    path('api/product/wishlist/list/<str:pk>/', WishlistAPIView.as_view()),
    path('api/product/wishlist/remove/<str:pk>/', WishlistRemoveAPIView.as_view()),
]