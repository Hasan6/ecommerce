import zipfile

from django.db import models
from settings.models import *
# from django.forms import ModelChoiceField

from category.models import Category
from settings.models import *
from django.contrib.auth.models import User
from users.models import City

# Create your models here.

method = (
    (1, 'DDP'),
    (2, 'Ex-Works')
)
status_info = (
    (1, 'Published'),
    (0, 'Unpublished'),
)


# class LeadTime(models.Model):
#     # Type: DDP / Ex-Works
#     type = models.CharField(max_length=100, unique=True)
#     description = models.TextField(blank=True)
#     days = models.IntegerField()
#     created_by = models.ForeignKey(
#         'auth.User',
#         on_delete=models.CASCADE
#     )
#
#     def __str__(self):
#         return "%s" % self.type

class Product(models.Model):
    name = models.CharField(max_length=250, unique=False)
    slug = models.SlugField(max_length=250)
    description = models.TextField(blank=True)
    category = models.ForeignKey(
        'category.Category',
        related_name="products",
        on_delete=models.CASCADE
    )
    unit_price = models.DecimalField(max_digits=10, decimal_places=2)
    delivery_method = models.IntegerField(choices=method, default=1)
    ddp_lead_time = models.IntegerField(default=0)
    ex_works_lead_time = models.IntegerField(default=0)
    commission = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    stock_quantity = models.IntegerField()
    unit = models.ForeignKey(
        'settings.Unit',
        related_name="products",
        on_delete=models.CASCADE, blank=True, null=True
    )
    color = models.ForeignKey(
        'settings.Color',
        related_name="products",
        on_delete=models.CASCADE, blank=True, null=True
    )
    brand = models.ForeignKey(
        'settings.Brand',
        related_name="products",
        on_delete=models.CASCADE, blank=True, null=True
    )
    seller = models.ForeignKey(
        'users.User',
        related_name="products",
        on_delete=models.CASCADE
    )
    image1 = models.ImageField(upload_to='uploads/product/images/', blank=True)
    image2 = models.ImageField(upload_to='uploads/product/images/', blank=True)
    status = models.IntegerField(choices=status_info, default=0)
    keyword = models.ManyToManyField(Keyword, blank=True,
                                     related_name="keyword", )
    is_deleted = models.BooleanField(default=False, verbose_name='Archived')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'product'
        verbose_name_plural = 'products'

    def get_absolute_url(self):
        return self.slug

    def __str__(self):
        return "%s" % self.name


class ProductAttachment(models.Model):
    path = models.FileField(upload_to='uploads/product/attachment/', null=True)
    product = models.ForeignKey(Product, blank=True, related_name='attachment', null=True, on_delete=models.CASCADE)

    def __str__(self):
        return "%s" % self.path


class PickupAddress(models.Model):
    product = models.ForeignKey(
        'Product',
        related_name="pickup_address",
        on_delete=models.CASCADE
    )
    city = models.ForeignKey(City, on_delete=models.PROTECT)
    address = models.TextField(blank=True)

    def __str__(self):
        return "%s" % self.city


class ProductBulkUpload(models.Model):
    path = models.FileField(upload_to='uploads/product/attachment/', null=True)

    def __str__(self):
        return "%s" % self.path

    def save(self, *args, **kwargs):
        if self.path:
            # opening the zip file in READ mode
            with zipfile.ZipFile(self.path, 'r') as zip:
                # printing all the contents of the zip file
                # zip.printdir()
                # extracting all the files
                print('Extracting all the files now...')
                zip.extractall()
                print('Done!')
        super(ProductBulkUpload, self).save(*args, **kwargs)


class ProductComment(models.Model):
    product = models.ForeignKey(
        'products.Product',
        related_name="comment_product",
        on_delete=models.CASCADE
    )
    buyer = models.ForeignKey(
        'users.User',
        related_name="comment_buyer",
        on_delete=models.CASCADE
    )
    comment = models.TextField(blank=True)
    status = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        'users.User',
        related_name="created_user",
        on_delete=models.CASCADE, blank=True, null=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_by = models.ForeignKey(
        'users.User',
        related_name="updated_user",
        on_delete=models.CASCADE, blank=True, null=True
    )
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.comment


class ProductCommentDetail(models.Model):
    comment = models.ForeignKey(
        ProductComment,
        related_name="reply_comment",
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(
        'users.User',
        related_name="reply_comment_user",
        on_delete=models.CASCADE
    )
    message = models.TextField(blank=True)
    message_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "%s" % self.message


class Wishlist(models.Model):
    product = models.ForeignKey(
        'products.Product',
        related_name="wish_product",
        on_delete=models.CASCADE
    )
    buyer = models.ForeignKey(
        'users.User',
        related_name="wish_buyer",
        on_delete=models.CASCADE
    )
    status = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        'users.User',
        related_name="created_by",
        on_delete=models.CASCADE, blank=True, null=True
    )
    created_at = models.DateTimeField(auto_now_add=True)
    updated_by = models.ForeignKey(
        'users.User',
        related_name="updated_by",
        on_delete=models.CASCADE, blank=True, null=True
    )
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s" % self.product
