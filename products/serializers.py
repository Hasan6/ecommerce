from collections import OrderedDict

from rest_framework import serializers
from products.models import Product, ProductAttachment, PickupAddress, ProductComment, ProductCommentDetail, Wishlist
from category.models import Category
from settings.models import Brand, Color, Unit, Keyword
# from django.contrib.auth.models import User
from users.models import User


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ['id', 'name']


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ['id', 'name']


class ColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Color
        fields = ['id', 'name']


class KeywordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Keyword
        fields = ['id', 'name']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'full_name', 'store_name', 'store_address']


class UnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = Unit
        fields = ['id', 'name']


class ProductAttachmentSerializer(serializers.ModelSerializer):
    path = serializers.SerializerMethodField('get_path')

    class Meta:
        model = ProductAttachment
        fields = ['id', 'path']

    def get_path(self, obj):
        if str(obj.path) != '':
            return '/media/' + str(obj.path)
        return None


class PickupAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = PickupAddress
        fields = ['id', 'city', 'address']


class PickupAddressesSerializer(serializers.ModelSerializer):
    class Meta:
        model = PickupAddress
        fields = ['id', 'city', 'address', 'product']


class ProductsSerializer(serializers.ModelSerializer):
    category = CategorySerializer()
    brand = BrandSerializer()
    color = ColorSerializer()
    unit = UnitSerializer()
    keyword = KeywordSerializer(many=True)
    attachment = ProductAttachmentSerializer(many=True, required=False)
    seller = UserSerializer()
    pickup_address = PickupAddressSerializer(many=True, required=False)
    image1 = serializers.SerializerMethodField('get_image1')
    image2 = serializers.SerializerMethodField('get_image2')

    class Meta:
        model = Product
        fields = ['id', 'category', 'attachment', 'pickup_address', 'brand', 'color', 'unit', 'seller', 'name', 'slug',
                  'keyword', 'description', 'unit_price', 'delivery_method', 'ddp_lead_time', 'ex_works_lead_time',
                  'commission', 'stock_quantity', 'status', 'created_at', 'image1', 'image2', ]

    def get_image1(self, obj):
        if str(obj.image1) != '':
            return '/media/' + str(obj.image1)
        return None

    def get_image2(self, obj):
        if str(obj.image2) != '':
            return '/media/' + str(obj.image2)
        return None


class ProductAddSerializer(serializers.ModelSerializer):
    attachment = ProductAttachmentSerializer(many=True, required=False)
    pickup_address = PickupAddressSerializer(many=True, required=False)

    class Meta:
        model = Product
        fields = ['id', 'category', 'attachment', 'pickup_address', 'brand', 'color', 'unit', 'seller', 'name', 'slug',
                  'description', 'unit_price', 'delivery_method', 'ddp_lead_time', 'ex_works_lead_time', 'commission',
                  'stock_quantity', 'status', 'created_at', 'image1', 'image2', 'keyword']

    def create(self, validated_data):
        attachment_data = 0
        pickupaddresses_data = 0
        # print(validated_data)
        keyword_data = validated_data.pop('keyword')
        # print(keyword_data)
        # print(type(keyword_data))
        if 'attachment' in validated_data.keys():
            attachment_data = validated_data.pop('attachment')
        if 'pickup_address' in validated_data.keys():
            pickupaddresses_data = validated_data.pop('pickup_address')
        product_data = Product.objects.create(**validated_data)
        product_data.keyword.set(keyword_data)
        if attachment_data is not 0:
            for attachment in attachment_data:
                ProductAttachment.objects.create(product=product_data, **attachment)
        if pickupaddresses_data is not 0:
            for pickup in pickupaddresses_data:
                PickupAddress.objects.create(product=product_data, **pickup)

        return product_data

    def update(self, instance, validated_data):
        attachment_data = 0
        pickup_data = 0
        if 'attachment' in validated_data.keys():
            attachment_data = validated_data.pop('attachment')
        if 'pickup_address' in validated_data.keys():
            pickup_data = validated_data.pop('pickup_address')

        keyword_data = validated_data.pop('keyword')
        instance.category = validated_data.get('category', instance.category)

        instance.brand = validated_data.get('brand', instance.brand)
        instance.color = validated_data.get('color', instance.color)
        instance.unit = validated_data.get('unit', instance.unit)
        instance.seller = validated_data.get('seller', instance.seller)
        instance.name = validated_data.get('name', instance.name)
        instance.slug = validated_data.get('slug', instance.slug)
        instance.description = validated_data.get('description', instance.description)
        instance.unit_price = validated_data.get('unit_price', instance.unit_price)
        instance.delivery_method = validated_data.get('delivery_method', instance.delivery_method)
        instance.ddp_lead_time = validated_data.get('ddp_lead_time', instance.ddp_lead_time)
        instance.ex_works_lead_time = validated_data.get('ex_works_lead_time', instance.ex_works_lead_time)
        instance.commission = validated_data.get('commission', instance.commission)
        instance.stock_quantity = validated_data.get('stock_quantity', instance.stock_quantity)
        instance.status = validated_data.get('status', instance.status)
        # instance.keyword = validated_data.get('keyword', instance.keyword)
        instance.image1 = validated_data.get('image1', instance.image1)
        instance.image2 = validated_data.get('image2', instance.image2)

        keep_attachment = []
        attachment_id = []
        if attachment_data is not 0:
            for attachment in attachment_data:
                c = ProductAttachment.objects.create(**attachment, product=instance)
                keep_attachment.append({'id': c.id, 'path': c.path, 'product': c.product})
                attachment_id.append(c.id)

            existing_attachments = ProductAttachment.objects.filter(product=instance)
            for attachment in existing_attachments:
                if attachment.id not in attachment_id:
                    attachment.delete()

        instance.attachments = keep_attachment

        keep_pickup = []
        pickup_id = []
        if pickup_data is not 0:
            for pickup in pickup_data:
                c = PickupAddress.objects.create(**pickup, product=instance)
                keep_pickup.append({'id': c.id, 'city': c.city, 'address': c.address, 'product': c.product})
                pickup_id.append(c.id)

            existing_pickups = PickupAddress.objects.filter(product=instance)
            for pickup in existing_pickups:
                if pickup.id not in pickup_id:
                    pickup.delete()

        instance.pickup_addresss = keep_pickup
        instance.keyword.set(keyword_data)
        instance.save()

        return instance


# Product Bulk Upload below:
class ModifiedProductAttachmentSerializer(serializers.ModelSerializer):
    path = serializers.CharField(max_length=None, required=False, allow_blank=True)

    class Meta:
        model = ProductAttachment
        fields = ['id', 'path']


class ModPickupAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = PickupAddress
        fields = ['id', 'city_id', 'address']


class SingleProductSerializer(serializers.ModelSerializer):
    attachment = ModifiedProductAttachmentSerializer(many=True, required=False)
    pickupaddresses = ModPickupAddressSerializer(many=True, required=False)
    image1 = serializers.CharField(max_length=None, required=False, allow_blank=True)
    image2 = serializers.CharField(max_length=None, required=False, allow_blank=True)

    class Meta:
        model = Product
        fields = ['id', 'category', 'attachment', 'pickupaddresses', 'brand', 'color', 'unit', 'seller', 'name', 'slug',
                  'description', 'unit_price', 'delivery_method', 'ddp_lead_time', 'ex_works_lead_time', 'commission',
                  'stock_quantity', 'image1', 'image2', 'keyword']

    def validate_category(self, value):
        if value:
            return value
        raise serializers.ValidationError('Invalid category')

    def validate_seller(self, value):
        if value:
            return value
        raise serializers.ValidationError('Invalid seller')

    def create(self, validated_data):
        attachment_data = 0
        pickupaddresses_data = 0
        # print("Created Portion")
        # print(validated_data)
        if 'attachment' in validated_data.keys():
            attachment_data = validated_data.pop('attachment')
        if 'pickupaddresses' in validated_data.keys():
            pickupaddresses_data = validated_data.pop('pickupaddresses')

        keyword_data = validated_data.pop('keyword')
        print(keyword_data)
        validated_data['category'] = Category(validated_data['category'])
        validated_data['seller'] = User(validated_data['seller'])
        if validated_data['brand'] is not None:
            validated_data['brand'] = Brand(validated_data['brand'])
        if validated_data['color'] is not None:
            validated_data['color'] = Color(validated_data['color'])
        if validated_data['unit'] is not None:
            validated_data['unit'] = Unit(validated_data['unit'])
        product_data = Product.objects.create(**validated_data)
        if attachment_data is not 0:
            for attachment in attachment_data:
                if attachment['path'] != '':
                    ProductAttachment.objects.create(product=product_data, **attachment)
        if pickupaddresses_data is not 0:
            for pickup in pickupaddresses_data:
                if pickup['city_id'] != '':
                    PickupAddress.objects.create(product=product_data, **pickup)
        for keyword in keyword_data:
            product_data.keyword.add(keyword)
        return product_data


# comments serializers

class CommentReplyListSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = ProductCommentDetail
        fields = ['id', 'user', 'message', 'message_date']


class CommentListSerializer(serializers.ModelSerializer):
    reply_comment = CommentReplyListSerializer(many=True)
    buyer = UserSerializer()

    class Meta:
        model = ProductComment
        fields = ['id', 'reply_comment', 'product', 'buyer', 'comment', 'created_at']


class CommentAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductComment
        fields = ['product', 'buyer', 'comment']


class CommentReplySerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductCommentDetail
        fields = ['comment', 'user', 'message']


class WishlistSerializer(serializers.ModelSerializer):
    buyer = UserSerializer()
    product = ProductsSerializer()
    class Meta:
        model = Wishlist
        fields = ['id', 'product', 'buyer', 'status', 'created_at']


class WishlistAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = Wishlist
        fields = ['product', 'buyer', 'status']
