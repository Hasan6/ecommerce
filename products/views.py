import os
from collections import OrderedDict

from django.http import JsonResponse
from rest_framework.parsers import MultiPartParser

from category.models import Category
from emdad.settings import BASE_DIR
from products.serializers import ProductsSerializer, ProductAddSerializer, \
    SingleProductSerializer, PickupAddressesSerializer, CommentAddSerializer, CommentReplySerializer, CommentListSerializer, WishlistSerializer, WishlistAddSerializer, CategorySerializer
from products.models import Product, PickupAddress, ProductComment, Wishlist
from rest_framework.views import APIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework import status
from django.db.models import Q, Count
from rest_framework.permissions import IsAuthenticated, AllowAny
from openpyxl import load_workbook

from settings.models import Commission, Keyword
from users.models import User
from orders.models import Order, OrderDetail

from django_filters.rest_framework import DjangoFilterBackend
from django_filters import FilterSet
from django_filters import rest_framework as filters
from rest_framework.filters import OrderingFilter, SearchFilter
# try:
#     default_commission = Commission.objects.get(pk=1)
# except:
#     default_commission = 0


class CustomPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000

    def get_paginated_response(self, data):
        return Response({
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Product List',
            'data': {
                'links': {
                    'next': self.get_next_link(),
                    'previous': self.get_previous_link()
                },
                'count': self.page.paginator.count,
                'page_size': self.page_size,
                'results': data
            }

        })


class CustomCommentsPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = 'page_size'
    max_page_size = 1000

    def get_paginated_response(self, data):
        return Response({
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Comments List',
            'data': {
                'links': {
                    'next': self.get_next_link(),
                    'previous': self.get_previous_link()
                },
                'count': self.page.paginator.count,
                'page_size': self.page_size,
                'results': data
            }

        })


class ProductListAPIView(ListAPIView, PageNumberPagination):
    permission_classes = (AllowAny,)
    queryset = Product.objects.filter(status=1,is_deleted=0)
    serializer_class = ProductsSerializer
    pagination_class = CustomPagination

    def get_queryset(self):
        data = Product.objects.filter(status=1,is_deleted=0)
        return data

    # def list(self, request):
    #     # Note the use of `get_queryset()` instead of `self.queryset`
    #     queryset = self.get_queryset()
    #     serializer = ProductsSerializer(queryset, many=True)
    #     page = self.paginate_queryset(serializer.data)
    #     return self.get_paginated_response(page)

class PopularProductListAPIView(APIView):
    permission_classes = (AllowAny,)

    def get(self,request):
        ordered_product = OrderDetail.objects.values('product').annotate(count=Count('product')).order_by('-count')[:12]
        product_info = []
        for d in ordered_product:
            for k, v in d.items():
                product = Product.objects.get(pk=v)
                product_info.append(product)

        serializer = ProductsSerializer(product_info, many=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Popular Product',
            'data': [serializer.data]
        }
        return Response(response)


class PopularCategoryListAPIView(APIView):
    permission_classes = (AllowAny,)

    def get(self,request):
        category_product = Product.objects.values('category').annotate(count=Count('id')).order_by('-count')[:6]
        # print(category_product.category)
        category_info = []
        for d in category_product:
            category = Category.objects.get(pk=d['category'])
            category_info.append(category)

        serializer = CategorySerializer(category_info, many=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Popular Category',
            'data': [serializer.data]
        }
        return Response(response)

class ProductOrderCheckAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,pk):
        ordered_product = OrderDetail.objects.filter(product_id=pk)
        if ordered_product:
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'Product ordered'
            }
            return Response(response)
        else:
            response = {
                'success': 'True',
                'status code': status.HTTP_404_NOT_FOUND,
                'message': 'Product not ordered'
            }
            return Response(response)

class ProductFilter(FilterSet):
    category = filters.CharFilter('category__name')
    brand = filters.CharFilter('brand__name')
    color = filters.CharFilter('color__name')
    min_price = filters.CharFilter(method='filter_by_min_price')
    max_price = filters.CharFilter(method='filter_by_max_price')

    class Meta:
        model = Product
        fields = ('category', 'brand')

    def filter_by_min_price(self, queryset, name, value):
        queryset = queryset.filter(unit_price__gt=value)
        return queryset

    def filter_by_max_price(self, queryset, name, value):
        queryset = queryset.filter(unit_price__lt=value)
        return queryset


class ProductFilterListAPIView(ListAPIView):
    permission_classes = (AllowAny,)
    queryset = Product.objects.filter(status=1,is_deleted=0)
    serializer_class = ProductsSerializer
    pagination_class = CustomPagination
    filter_backends = (DjangoFilterBackend, SearchFilter, OrderingFilter)
    search_fields = ('name', 'category__name', 'brand__name')
    # filter_fields = ('category__name', 'brand__name')
    filter_class = ProductFilter
    ordering_fields = ('unit_price',)

    def get_queryset(self):
        data = Product.objects.filter(status=1,is_deleted=0)
        return data

class ProductDetailsAPIView(APIView):
    permission_classes = (AllowAny,)

    def GetProductById(self, id):
        try:
            model = Product.objects.filter(id=id)
            return model
        except Product.DoesNotExist:
            return

    def get(self, request, pk):
        id = pk
        if not self.GetProductById(id):
            response = {
                'success': 'True',
                'status code': status.HTTP_404_NOT_FOUND,
                'message': 'Product Plan ' + id + ' is Not Found'
            }
            return Response(response)
        product_details = Product.objects.get(id=pk)
        serializer = ProductsSerializer(product_details, many=False)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Product Details',
            'data': [serializer.data]
        }
        return Response(response)


class ProductBySellerAPIView(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = ProductsSerializer
    pagination_class = CustomPagination

    def get_queryset(self):
        id = self.kwargs['pk']
        products = Product.objects.filter(seller=id,is_deleted=0)
        return products


class CategoryWiseProductAPIView(ListAPIView):
    permission_classes = (AllowAny,)
    serializer_class = ProductsSerializer
    pagination_class = CustomPagination

    def get_queryset(self):
        id = self.kwargs['pk']
        products = Product.objects.filter(category=id,status=1,is_deleted=0)
        return products

class SimilarProductsListAPIView(ListAPIView):
    permission_classes = (AllowAny,)
    def GetProductBySellerId(self, id):
        try:
            model = Product.objects.filter(category=id)
            return model
        except Product.DoesNotExist:
            return

    def get(self, request, pk):
        id = pk
        if not self.GetProductBySellerId(id):
            response = {
                'success': 'True',
                'status code': status.HTTP_404_NOT_FOUND,
                'message': 'Category Product Not Found'
            }
            return Response(response)
        products = Product.objects.filter(category=id,status=1,is_deleted=0).order_by('-id')[:15]
        serializer = ProductsSerializer(products, many=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Similar Product List',
            'data': [serializer.data]
        }
        return Response(response)



class ProductFilteringAPIView(ListAPIView):
    serializer_class = ProductsSerializer
    pagination_class = CustomPagination

    def get_queryset(self):
        query = self.kwargs['query']
        products = Product.objects.all().filter(
            Q(name__contains=query) | Q(description__contains=query) | Q(slug__contains=query))
        return products


class ProductInsertAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Product Insert Successful',
            'data': []
        }
        serializer = ProductAddSerializer(data=request.data)
        user = User.objects.get(id=self.request.user.id)
        # print(type(user.is_subscribed))
        subscribed = user.is_subscribed
        if subscribed:
            if serializer.is_valid():
                serializer.save()
                response['data'] = [serializer.data]
                return Response(response)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            response['message'] = 'User not subscribed'
            return Response(response)


class UpdateProductAPIView(APIView):
    permission_classes = [IsAuthenticated]

    def get_object(self, pk):
        try:
            model = Product.objects.filter(pk=pk)
            return model
        except Product.DoesNotExist:
            return

    def put(self, request, pk, format=None):
        product = Product.objects.get(id=pk)
        serializer = ProductAddSerializer(product, data=request.data)
        if serializer.is_valid():
            serializer.save()

            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'Product Updated Successful',
                'data': serializer.data
            }

            return Response(response, status=status.HTTP_200_OK)

        response = {
            'success': 'False',
            'status code': status.HTTP_400_BAD_REQUEST,
            'message': 'Product Update Failed',
            'data': []
        }

        return Response(response, status=status.HTTP_400_BAD_REQUEST)


class DeleteProductAPIView(APIView):
    def GetProductById(self, id):
        try:
            model = Product.objects.filter(id=id)
            return model
        except Product.DoesNotExist:
            return

    def get(self, request, pk):
        id = pk
        if not self.GetProductById(id):
            response = {
                'success': 'True',
                'status code': status.HTTP_404_NOT_FOUND,
                'message': 'Product Plan ' + id + ' is Not Found'
            }
            return Response(response)

        if self.GetProductById(id):
            Product.objects.filter(pk=id).update(is_deleted='1')
            response = {
                'success': 'True',
                'status code': status.HTTP_202_ACCEPTED,
                'message': 'Product deleted successful'
            }
            return Response(response)


def convert_to_keyword_list(a):
    comma_separated_str = a.split(',')
    # print(comma_separated_str)
    try:
        return [int(b) for b in comma_separated_str]
    except ValueError:
        return []


class BulkUploadView(APIView):
    parser_class = (MultiPartParser,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, format=None):
        # to access files
        # print(request.FILES)
        # to access data
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Product Insert Successful',
            'data': []
        }
        try:
            default_commission = Commission.objects.get(pk=1)
        except:
            default_commission = 0
        file_obj = request.data["file"]
        data = load_workbook(file_obj)
        sheet = data.active
        excel_data = list()
        user = User.objects.get(id=self.request.user.id)
        # print(type(user.is_subscribed))
        subscribed = user.is_subscribed
        if subscribed:
            for row in sheet.iter_rows():
                row_data = list()
                for cell in row:
                    row_data.append(str(cell.value))
                excel_data.append(row_data)
            excel_data.pop(0)
            # print(excel_data)

            commission_value = float(str(default_commission))
            # print(commission_value)
            product_objs = [{
                "name": row[0],
                "slug": row[1],
                "description": row[2] if row[3] != 'None' else '',
                "category": int(row[3]) if row[3] != 'None' else '',
                "unit_price": float(row[4]) if row[4] != 'None' else '',
                "delivery_method": int(row[5]) if row[5] != 'None' else 1,
                "ddp_lead_time": float(row[6]) if row[6] != 'None' else 0,
                "ex_works_lead_time": float(row[7]) if row[7] != 'None' else 0,
                "commission": (float(row[4])*commission_value*.01) if row[4] != 'None' else 0,
                "stock_quantity": int(row[8]) if row[8] != 'None' else 0,
                'unit': int(row[9]) if row[9] != 'None' else None,
                'color': int(row[10]) if row[10] != 'None' else None,
                'brand': int(row[11]) if row[11] != 'None' else None,
                "seller": int(row[12]) if row[12] != 'None' else '',
                "image1": row[13] if row[13] != 'None' else '',
                "image2": row[14] if row[14] != 'None' else '',
                "attachment": [
                    {"path": row[15] if row[15] != 'None' else ''},
                    {"path": row[16] if row[16] != 'None' else ''},
                    {"path": row[17] if row[17] != 'None' else ''}
                ],
                "pickupaddresses": [
                    {"city_id": int(row[18]) if row[18] != 'None' else '',
                     "address": row[19] if row[19] != 'None' else ''
                     }
                ],
                "keyword": convert_to_keyword_list(row[20])
            } for row in excel_data]
            # print(product_objs)
            serializer_error = False
            for item in product_objs:
                serializer = SingleProductSerializer(data=item)
                if serializer.is_valid(raise_exception=True):
                    serializer.create(item)
                else:
                    serializer_error = True
                    # print(serializer.errors)
                    response['message'] = 'Please check the file'
                    response['data'] = serializer.errors
                    return Response(response)

            return Response(response)
        else:
            response['message'] = 'User not subscribed'
            return Response(response)


class PickupAddressView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self, request, pk):
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Pickup address found',
            'data': []
        }
        pickup = PickupAddress.objects.filter(product=pk)
        # print(pickup)
        serializer = PickupAddressesSerializer(pickup, many=True)
        response['data'] = serializer.data
        # response['message'] = 'Pickup address not found'
        return Response(response)


class CommentProductAPIView(APIView):
    permission_classes = (AllowAny,)

    def post(self, request):
        buyer_id = request.data.get('buyer')
        product_id = request.data.get('product')
        order_details = 0
        order = Order.objects.filter(buyer=buyer_id)
        for order_data in order:
            order_details = OrderDetail.objects.filter(product=product_id, order=order_data.id).count()

        if order_details > 0:
            serializer = CommentAddSerializer(data=request.data)

            if serializer.is_valid():
                # serializer.data.vat_amount = vat_amount
                serializer.save()
                response = {
                    'success': 'True',
                    'status code': status.HTTP_200_OK,
                    'message': 'Comments Added Successful',
                    'data': [serializer.data]
                }
                return Response(response)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'You haven not buy this product yet.'
            }
            return Response(response)
class CommentReplyAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):

        seller_id = request.data.get('user')
        comment_id = request.data.get('comment')
        comment_info = ProductComment.objects.get(id=comment_id)
        seller_check = Product.objects.filter(seller=seller_id, id=comment_info.product_id).count()

        if seller_check > 0:
            serializer = CommentReplySerializer(data=request.data)

            if serializer.is_valid():
                # serializer.data.vat_amount = vat_amount
                serializer.save()

                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'Seller Does not exist'
            }
            return Response(response)

class ProductCommentListAPIView(ListAPIView):
    permission_classes = (AllowAny,)

    serializer_class = CommentListSerializer
    pagination_class = CustomCommentsPagination


    def get_queryset(self):
        id = self.kwargs['pk']
        comments = ProductComment.objects.filter(product=id)
        return comments

class WishlistAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,pk):
        if self.request.user.id != int(pk):
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'You can not show this list!!'
            }
            return Response(response)
        wish = Wishlist.objects.filter(buyer=pk)
        serializer = WishlistSerializer(wish, many=True)
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Wishlist',
            'data': [serializer.data]
        }
        return Response(response)

class WishlistRemoveAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,pk):
        Wishlist.objects.filter(pk=pk).delete()
        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Wishlist Deleted Successful'
        }
        return Response(response)

class WishlistStatusAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def get(self,request,pk):
        wishlist = Wishlist.objects.filter(product_id=pk)
        if wishlist:
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'status': '0'
            }
            return Response(response)
        else:
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'status': '1'
            }
            return Response(response)

class WishlistAddAPIView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self, request):

        product_id = request.data['product']
        buyer_id = request.data['buyer']
        wish = Wishlist.objects.filter(product=product_id,buyer=buyer_id)
        if wish:
            response = {
                'success': 'True',
                'status code': status.HTTP_200_OK,
                'message': 'Already Added'
            }
            return Response(response)
        else:
            serializer = WishlistAddSerializer(data=request.data)

            if serializer.is_valid():
                # serializer.data.vat_amount = vat_amount
                serializer.save()

                response = {
                    'success': 'True',
                    'status code': status.HTTP_200_OK,
                    'message': 'Wishlist Insert Successful',
                    'data' : [serializer.data]
                }
                return Response(response)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class SellerCategoryStoreInfoAPIView(APIView):
    permission_classes = (AllowAny,)

    def get(self,request,pk):
        # print(type(pk))
        seller_info = Product.objects.values('category','seller').annotate(count=Count('category')).filter(seller=pk,status=1,is_deleted=0).order_by()
        category_info = []
        for d in seller_info:
            for k,v in d.items():
                if k == "category":
                    cat = Category.objects.get(pk=v)
                    category = {'id':cat.id,'name':cat.name}
                    category_info.append(category)

        sell = User.objects.get(pk=pk)

        if sell.profile_pic:
            profile_picture = sell.profile_pic
        else:
            profile_picture = ''


        if sell.store_pic:
            store_picture = sell.store_pic
        else:
            store_picture = ''

        seller_inf = [{'id': sell.id, 'full_name': sell.full_name, 'phone': sell.phone, 'address': sell.address,
                      'profile_pic': str(profile_picture), 'store_name': sell.store_name,
                      'store_address': sell.store_address, 'store_pic': str(store_picture), 'date_joined': sell.date_joined, 'category_info':category_info}]

        response = {
            'success': 'True',
            'status code': status.HTTP_200_OK,
            'message': 'Store and category information',
            'data': seller_inf
        }
        return Response(response)