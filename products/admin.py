from django.contrib import admin
from products.forms import ProductForm
from products.models import Product, PickupAddress, ProductAttachment, ProductBulkUpload, ProductComment, ProductCommentDetail, Wishlist
from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
from import_export.admin import ImportExportModelAdmin
from products.resources import ProductResource
# Register your models here.


class ProductAttachmentAdmin(admin.StackedInline):
    model = ProductAttachment

class PickupAddressAdmin(admin.StackedInline):
    model = PickupAddress


class ProductAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    form = ProductForm
    inlines = [ProductAttachmentAdmin, PickupAddressAdmin]
    list_display = ('name','category', 'delivery_method', 'commission', 'stock_quantity', 'brand', 'seller', 'status')
    list_filter = ('status', 'seller', 'category', 'brand', ('created_at', DateRangeFilter))
    prepopulated_fields = {'slug': ('name',)}
    search_fields = ('name',)
    actions = ('set_products_to_published',)

    def set_products_to_published(self, request, queryset):
        count = queryset.update(status=False)
        self.message_user(request, '{} products have been published.'.format(count))
    set_products_to_published.short_description = 'Mark selected products as published'

    resource_class = ProductResource

class ProductCommentDetailAdmin(admin.StackedInline):
    model = ProductCommentDetail

class ProductCommentAdmin(admin.ModelAdmin):
    exclude = ['created_by', 'updated_by']
    inlines = [ProductCommentDetailAdmin,]
    list_display = (
    'product', 'buyer', 'comment', 'status', 'created_at',)

    def save_model(self, request, obj, form, change):
        if not obj.id:
            obj.created_by = request.user
            obj.updated_by = request.user
        if change and obj.id:
            obj.updated_by = request.user
        obj.save()


class WishlistAdmin(admin.ModelAdmin):
    exclude = ['created_by', 'updated_by']
    list_display = (
    'product', 'buyer', 'status', 'created_at',)

    def save_model(self, request, obj, form, change):
        if not obj.id:
            obj.created_by = request.user
            obj.updated_by = request.user
        if change and obj.id:
            obj.updated_by = request.user
        obj.save()

admin.site.register(Product, ProductAdmin)
# admin.site.register(PickupAddress)
admin.site.register(ProductBulkUpload)
admin.site.register(ProductComment,ProductCommentAdmin)
admin.site.register(Wishlist,WishlistAdmin)