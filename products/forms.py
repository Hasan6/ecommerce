from django import forms

from products.models import Product
# from settings.models import Commission

# get_commission = Commission.objects.all()
# if get_commission.count() > 0:
#     default_commission = [data for data in get_commission][0]
# else:
#     default_commission = 0


class ProductForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # print(self.fields)
        # self.fields['commission'].widget.attrs['readonly'] = True
        # self.initial['commission'] = default_commission
        self.fields['commission'].label = "Commission(%)"
    class Meta:
        model = Product
        fields = '__all__'
