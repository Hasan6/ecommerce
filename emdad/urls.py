"""emdad URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from django.conf.urls.static import static
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token
from rest_framework.documentation import include_docs_urls
from rest_framework.permissions import IsAuthenticated, AllowAny

from emdad import settings

admin.site.site_header = 'Emdad Admin Dashboard'
admin.site.site_title = 'Emdad Admin Dashboard'
admin.site.index_title = 'Emdad Dashboard Administration'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('category/', include("category.urls")),
    path('', include("buyer.urls")),
    path('', include("seller.urls")),
    path('', include("users.urls")),
    path('', include("subscriptions.urls")),
    path('', include("products.urls")),
    path('', include("coupons.urls")),
    path('', include("settings.urls")),
    path('', include("orders.urls")),
    path('', include("quotation.urls")),
    path('', include("issues.urls")),
    path('summernote/', include('django_summernote.urls')),
    # JWT Token
    # path('auth/login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    # path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    # path('auth/login/', obtain_jwt_token),
    path('auth/refresh-token/', refresh_jwt_token),
    # JWT Token
    # Forgot Password
    path('api/password_reset/', include('django_rest_passwordreset.urls', namespace='password_reset')),
    # Forgot Password

    # API Docs
    path('docs/', include_docs_urls(title="EMDAD", description="An E-commerce platform", permission_classes=(AllowAny,)))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
